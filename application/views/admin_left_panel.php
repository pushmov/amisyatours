		<div class="left-navigation float-left" id="left-panel">
			<div id="accordian">
				<ul>
					<li class="active">
						<h3>Dashboard</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>dashboard/admin/">Dashboard</a></li>
							<li><a href="<?php echo base_url(); ?>welcome/admin_logout/">Logout</a></li>
						</ul>
					</li>
					<li>
						<h3>Haji</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>haji/admin/">Mengenai Haji</a></li>
							<li><a href="<?php echo base_url(); ?>haji/admin/persyaratan/">Persyaratan & Ketentuan</a></li>
							<li><a href="<?php echo base_url(); ?>haji/admin/biaya/">Biaya</a></li>
						</ul>
					</li>
					<li>
						<h3>Umrah</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>umrah/admin/">Mengenai Umrah</a></li>
							<li><a href="<?php echo base_url(); ?>umrah/admin/persyaratan/">Persyaratan & Ketentuan</a></li>
							<li><a href="<?php echo base_url(); ?>umrah/admin/biaya/">Biaya</a></li>
						</ul>
					</li>
					<li>
						<h3>Paket Tours</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>pages/admin/domestik">Domestik</a></li>
							<li><a href="<?php echo base_url(); ?>pages/admin/luar_negri/">Luar Negri</a></li>
							<li><a href="<?php echo base_url(); ?>pages/admin/rental/">Rental Bus</a></li>
						</ul>
					</li>
					<li>
						<h3>Pages</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>pages/admin/antar_jemput/">Layanan Antar Jemput</a></li>
							<li><a href="<?php echo base_url(); ?>pages/admin/prosedur/">Prosedur Pemesanan</a></li>
						</ul>
					</li>
					<!-- we will keep this LI open by default -->
					
					<li>
						<h3>Members</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>user/admin/">Semua Member</a></li>
							<li><a href="<?php echo base_url(); ?>user/admin/add/">Add Member</a></li>
							<li><a href="<?php echo base_url(); ?>user/admin/profile/">Your Profile</a></li>
						</ul>
					</li>
					<li>
						<h3>Products</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>product/admin/">Semua Product</a></li>
							<li><a href="<?php echo base_url(); ?>product/admin/add/">Add Product</a></li>
						</ul>
					</li>
					<li>
						<h3>Gallery</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>gallery/admin/">Semua Gallery</a></li>
							<li><a href="<?php echo base_url(); ?>gallery/admin/add/">Add Gallery</a></li>
						</ul>
					</li>
					<li>
						<h3>Dokumen</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>passport/admin/">Passport</a></li>
						</ul>
					</li>
					<li>
						<h3>Berita</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>berita/admin/">Semua Berita</a></li>
							<li><a href="<?php echo base_url(); ?>berita/admin/add/">Add Berita</a></li>
						</ul>
					</li>
					<li>
						<h3>Website Options</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>options/admin/">Options</a></li>
							<li><a href="<?php echo base_url(); ?>slider/admin/">Slider</a></li>
							<li><a href="<?php echo base_url(); ?>options/admin/socmed/">Social Media</a></li>
							<li><a href="<?php echo base_url(); ?>options/admin/category/">Category</a></li>
						</ul>
					</li>
					<!--
					<li>
						<h3>Manage Posts</h3>
						<ul>
							<li><a href="http://localhost/tubes-psi/admin_posts/">Lihat Posts</a></li>
							<li><a href="http://localhost/tubes-psi/admin_posts/add/">Tambah Post</a></li>
						</ul>
					</li>
					
					<li>
						<h3>Pages</h3>
						<ul>
							<li><a href="#">All Pages</a></li>
							<li><a href="#">Add Pages</a></li>
						</ul>
					</li>
					<li>
						<h3>Data Kereta Api</h3>
						<ul>
							<li><a href="http://localhost/tubes-psi/admin_ka/">Lihat Data Kereta Api</a></li>
							<li><a href="http://localhost/tubes-psi/admin_ka/add/">Tambah Data</a></li>
						</ul>
					</li>
					<li>
						<h3>Data Rute Kereta Api</h3>
						<ul>
							<li><a href="http://localhost/tubes-psi/admin_rute_ka/">Lihat Rute Kereta Api</a></li>
							<li><a href="http://localhost/tubes-psi/admin_rute_ka/add/">Tambah Data</a></li>
						</ul>
					</li>
					<li>
						<h3>Data Stasiun</h3>
						<ul>
							<li><a href="http://localhost/tubes-psi/admin_stasiun_ka/">Lihat Data Stasiun</a></li>
							<li><a href="http://localhost/tubes-psi/admin_stasiun_ka/add/">Tambah Data</a></li>
						</ul>
					</li>
					<li>
						<h3>Data Transaksi</h3>
						<ul>
							<li><a href="http://localhost/tubes-psi/admin_transaksi/">Completed</a></li>
							<li><a href="http://localhost/tubes-psi/admin_transaksi/pending/">Pending</a></li>
						</ul>
					</li>
				-->
				</ul>
			</div>
		</div>