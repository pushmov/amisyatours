		<section class="footer-content clearfix">
			<div class="center">

				<?php if(!empty($socmed)) : ?>
				<div class="socmed-wrapper">
					<?php foreach($socmed as $row) : ?>
					<div class="socmed" id="<?php echo $row->name; ?>" <?php echo ($row->target != '') ? 'alt="'.$row->target.'"' : ''?>>
						<img src="<?php echo base_url(); ?>public/images/socmed/colour/<?php echo $row->icon; ?>" class="bottom">
						<img src="<?php echo base_url(); ?>public/images/socmed/grey/<?php echo $row->icon; ?>" class="top">
					</div>
					<?php endforeach;?>
				</div>
				<?php else : ?>
				<div class="socmed-wrapper">
					<div class="socmed" id="rss">
						<img src="<?php echo base_url(); ?>public/images/socmed/colour/rss.png" class="bottom">
						<img src="<?php echo base_url(); ?>public/images/socmed/grey/rss.png" class="top">
					</div>
					
					<div class="socmed" id="facebook">
						<img src="<?php echo base_url(); ?>public/images/socmed/colour/facebook.png" class="bottom">
						<img src="<?php echo base_url(); ?>public/images/socmed/grey/facebook.png" class="top">
					</div>
					
					<div class="socmed" id="twitter">
						<img src="<?php echo base_url(); ?>public/images/socmed/colour/twitter.png" class="bottom">
						<img src="<?php echo base_url(); ?>public/images/socmed/grey/twitter.png" class="top">
					</div>
					
					<div class="socmed" id="youtube">
						<img src="<?php echo base_url(); ?>public/images/socmed/colour/you_tube.png" class="bottom">
						<img src="<?php echo base_url(); ?>public/images/socmed/grey/you_tube.png" class="top">
					</div>
					
					<div class="socmed" id="gplus">
						<img src="<?php echo base_url(); ?>public/images/socmed/colour/google+.png" class="bottom">
						<img src="<?php echo base_url(); ?>public/images/socmed/grey/google+.png" class="top">
					</div>
				</div>
				<?php endif; ?>
			</div>

			<div class="float-left" style="width:100%">
				<div class="clearfix">
					<div class="cols-1 float-left" style="width:23%;margin-right:20px;">
						<!-- left panel here -->
						<h3>Menu</h3>
						<ul class="footer-menu">
							<li><a style="color:#333333 !important" href="<?php echo base_url(); ?>">Home</a></li>
							<li><a style="color:#333333 !important" href="#contactus-popup-display" class="contact-modal-open" rel="modal:open" id="contactus-popup">Contact</a></li>
							<li><a style="color:#333333 !important" href="<?php echo base_url(); ?>gallery/">Gallery</a></li>
						</ul>
					</div>
					<div class="cols-1 float-left" style="width:23%;margin-right:20px;">
						<h3>Haji</h3>
						<ul class="footer-menu">
							<li><a style="color:#333333 !important" href="<?php echo base_url() . $slug['haji']['about']; ?>">Tentang Haji</a></li>
							<li><a style="color:#333333 !important" href="<?php echo base_url() . $slug['haji']['persyaratan']; ?>">Persyaratan Haji</a></li>
							<li><a style="color:#333333 !important" href="<?php echo base_url() . $slug['haji']['biaya']; ?>">Biaya Haji</a></li>
						</ul>
					</div>
					<div class="cols-2 float-left" style="width:23%">
						<!-- center panel here -->
						<h3>Umrah</h3>
						<ul class="footer-menu">
							<li><a style="color:#333333 !important" href="<?php echo base_url() . $slug['umrah']['about']; ?>">Tentang Umrah</a></li>
							<li><a style="color:#333333 !important" href="<?php echo base_url() . $slug['umrah']['persyaratan']; ?>">Persyaratan Umrah</a></li>
							<li><a style="color:#333333 !important" href="<?php echo base_url() . $slug['umrah']['biaya']; ?>">Biaya Umrah</a></li>
						</ul>
					</div>
					<div class="cols-2 float-left" style="width:23%">
						<!-- center panel here -->
						<h3>Contact Us</h3>
						<div style="padding:8px 0"><i class="fa fa-home fa-lg" style="margin-right:5px;"></i>Komplek Ruko Pasar Modern Batu Nunggal Blok RD 35 Bandung - Indonesia</div>
						<div style="padding:8px 0"><i class="fa fa-phone fa-lg" style="margin-right:5px;"></i> (022) 87526751</div>
						<div style="padding:8px 0"><i class="fa fa-envelope fa-lg" style="margin-right:5px;"></i><a style="color:#333333 !important" href="mailto:amisyatours@yahoo.com">amisyatours@yahoo.com</a></div>
					</div>
				</div>
			</div>
		</section>
		<section class="end-content clearfix center">
			<h2 style="color:#11B997">amisya.com</h2>
			<div style="margin:0 150px">
				<p class="fs12" style="font-weight:400">"Amisya Tours & Travel adalah biro perjalanan Umrah dan ONH untuk umat muslim yang hendak menunaikan Umrah dan Haji. Kami juga melayani pemesanan Tiket Domestic dan International."</p>
			</div>
		</section>

		<script>
		$(document).ready(function(){
			$('ul.open').hide();
			$('body').click(function(){
				if($('.fixed-nav-right').css('right') == '170px'){
					$('li.show-frame').css({background : '#5E5E5E'});
					$('.fixed-nav-right').animate({right:"-=170"});
					$('.social-streamer').animate({right:"-=170"});
				}
			});
		});
		$('div.socmed').click(function(){
			var url = $(this).attr('alt');
			window.location=url;
		});

		$('a.contact-modal-open').click(function(event) {
			$(this).modal({
				fadeDuration: 250
			});
			return false;
		});

		$('#contactnow').click(function(){
			
			var email = $('#name').val();
			var name = $('#email').val();
			var message = $('#message').val();

			if(email == ''){
				return false;
			}
			if(name == ''){
				return false;
			}
			if(message == ''){
				return false;
			}

			$(this).removeClass('btn-primary').addClass('btn-danger').html("Processing");

			var bar = $('.bar');
			var percent = $('.percent');
			var status = $('#status');
			$('.progress').show();

			setTimeout(function(){
				$('#contact-form').ajaxSubmit({
					dataType:  'json', 
					url: '<?php echo base_url(); ?>welcome/contact/post/',
					beforeSend: function() {
						status.empty();
						var percentVal = '0%';
						bar.width(percentVal)
						percent.html(percentVal);
					},
					
					uploadProgress: function(event, position, total, percentComplete) {
						var percentVal = percentComplete + '%';
						bar.width(percentVal)
						percent.html(percentVal);
					},
					
					success: processJson
				});
				return false;
			},3000);
		});

		function processJson(data){
			$('#contactnow').removeClass('btn-danger').addClass('btn-primary').html("Send");
			$('#status').html(data.message);
			$('#name').val('');
			$('#email').val('');
			$('#message').val('');
			Recaptcha.reload();
		}
		</script>

		<script>
		$(document).ready(function(){

			$('.product-wrapper').hover(function(){
				$(this).find('div').show('fast');
			},function(){
				$(this).find('div').hide('fast');
			});

		});

		$('li.show-frame').click(function(){

			var classname = $(this).attr('class');
			var color = classname.replace("show-frame ","");
			var bgcolor,border,content;
			var border_default = '3px solid transparent';
			var prev_content = $('.social-streamer').attr('alt');

			$('li.frame-content').hide();

			switch(color){
				case 'purple':
					bgcolor = '#580361';
					border = '3px solid #580361';
					content = 'ym';
					break;
				case 'blue':
					bgcolor = '#3B5998';
					border = '3px solid #3B5998';
					content = 'fb';
					break;
				case 'skyblue':
					bgcolor = '#4EC2DC';
					border = '3px solid #4EC2DC';
					content = 'tw';
					break;
				case 'red':
					bgcolor = '#DF1F1C';
					border = '3px solid #DF1F1C';
					content = 'yt';
					break;
			}
			$('.social-streamer').attr('alt',content);

			$(this).css({background : bgcolor});
			$('.social-streamer').css({border: border})

			if($('.fixed-nav-right').css('right') == '0px'){
				$('.fixed-nav-right').animate({right:"+=170"});
				$('.social-streamer').animate({right:"+=170"});
				$('li.'+content).show();
			} else if($('.fixed-nav-right').css('right') == '170px'){


				if(prev_content != content){
					$('li.show-frame').css({background : '#5E5E5E'});
					$(this).css({background : bgcolor});
					$('li.frame-content').hide();
					$('li.'+content).show();
					$('.social-streamer').css({border : border});	
				} else {
					$('.fixed-nav-right').animate({right:"-=170"});
					$('.social-streamer').animate({right:"-=170"});
					$('li.show-frame').css({background : '#5E5E5E'});
					$('li.frame-content').hide();
					$('.social-streamer').css({border : border});	
				}
				
			}
			

		});
		</script>
		<script>

		</script>

