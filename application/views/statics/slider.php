				
				<?php 
					$anim = array("moveFromLeft", "moveFomRight", "moveFromTop", "moveFromBottom", "fadeIn", "fadeFromLeft", "fadeFromRight", "fadeFromTop", "fadeFromBottom"); 
					$rand_keys = array_rand($anim, 9);
				?>
				<section>
					<div class="clearfix">
						<div class="camera_wrap camera_azure_skin" id="camera_wrap_1">
							<?php if(!empty($sliders)) : ?>
							<?php $counter = 0; ?>
							<?php foreach($sliders as $slide) : ?>
								<?php if(!file_exists('./public/images/slides/'.$slide->slides_name) || $slide->slides_name == '') continue; ?>
								<div data-thumb="<?php echo base_url()?>public/plugin/imagecache.php?width=100&amp;height=75&amp;cropratio=1:1&image=<?php echo base_url(); ?>public/images/slides/thumbs/<?php echo $slide->slides_name; ?>" data-src="<?php echo base_url()?>public/plugin/imagecache.php?width=1280&amp;height=800&amp;cropratio=1:1&image=<?php echo base_url()?>public/images/slides/<?php echo $slide->slides_name; ?>">
		            			<?php if($slide->slides_caption != '') : ?>
			            			<div class="camera_caption <?php echo $anim[$rand_keys[$counter]]; ?>">
			            				<?php echo $slide->slides_caption; ?>
			            			</div>
		            			<?php endif; ?>
		            			</div>
		            		<?php $counter++; ?>
							<?php endforeach; ?>

							<?php else : ?>

		            <div data-thumb="<?php echo base_url()?>public/plugin/imagecache.php?width=100&amp;height=75&amp;cropratio=1:1&image=<?php echo base_url(); ?>public/images/slides/thumbs/new1.jpg" data-src="<?php echo base_url()?>public/plugin/imagecache.php?width=1280&amp;height=800&amp;cropratio=1:1&image=<?php echo base_url()?>public/images/slides/new1.jpg">
		            		<div class="camera_caption <?php echo $anim[$rand_keys[0]]; ?>">Txt Caption 1</div>
		            </div>
		            <div data-thumb="<?php echo base_url()?>public/plugin/imagecache.php?width=100&amp;height=75&amp;cropratio=1:1&image=<?php echo base_url(); ?>public/images/slides/thumbs/new2.jpg" data-src="<?php echo base_url()?>public/plugin/imagecache.php?width=1280&amp;height=800&amp;cropratio=1:1&image=<?php echo base_url()?>public/images/slides/new2.jpg">
		            		<div class="camera_caption <?php echo $anim[$rand_keys[1]]; ?>">Txt Caption 2</div>
		            </div>
		            <div data-thumb="<?php echo base_url()?>public/plugin/imagecache.php?width=100&amp;height=75&amp;cropratio=1:1&image=<?php echo base_url(); ?>public/images/slides/thumbs/new3.jpg" data-src="<?php echo base_url()?>public/plugin/imagecache.php?width=1280&amp;height=800&amp;cropratio=1:1&image=<?php echo base_url()?>public/images/slides/new3.jpg">
		            		<div class="camera_caption <?php echo $anim[$rand_keys[2]]; ?>">Txt Caption 3</div>
		            </div>
		            <div data-thumb="<?php echo base_url()?>public/plugin/imagecache.php?width=100&amp;height=75&amp;cropratio=1:1&image=<?php echo base_url(); ?>public/images/slides/thumbs/new4.jpg" data-src="<?php echo base_url()?>public/plugin/imagecache.php?width=1280&amp;height=800&amp;cropratio=1:1&image=<?php echo base_url()?>public/images/slides/new4.jpg">
		            		<div class="camera_caption <?php echo $anim[$rand_keys[3]]; ?>">Txt Caption 4</div>
		            </div>
		            <div data-thumb="<?php echo base_url()?>public/plugin/imagecache.php?width=100&amp;height=75&amp;cropratio=1:1&image=<?php echo base_url(); ?>public/images/slides/thumbs/new5.jpg" data-src="<?php echo base_url()?>public/plugin/imagecache.php?width=1280&amp;height=800&amp;cropratio=1:1&image=<?php echo base_url()?>public/images/slides/new5.jpg">
		            		<div class="camera_caption <?php echo $anim[$rand_keys[4]]; ?>">Txt Caption 5</div>
		            </div>
		            <div data-thumb="<?php echo base_url()?>public/plugin/imagecache.php?width=100&amp;height=75&amp;cropratio=1:1&image=<?php echo base_url(); ?>public/images/slides/thumbs/new6.jpg" data-src="<?php echo base_url()?>public/plugin/imagecache.php?width=1280&amp;height=800&amp;cropratio=1:1&image=<?php echo base_url()?>public/images/slides/new6.jpg">
		            		<div class="camera_caption <?php echo $anim[$rand_keys[5]]; ?>">Txt Caption 6</div>
		            </div>
	          	<?php endif; ?>
	        	</div>
					</div>
				</section>