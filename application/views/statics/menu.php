<body>
<!--
	<section class="center">
		<div id="header">
			<div class="inner" id="menuclick">menu</div>

			<div class="inner left_area" id="left">
		    <a href="#">Edit Profile</a>
		    <a href="#">Settings</a>
		    <a href="?logoff">Sign Out</a>
		</div>
		</div>
	</section>
-->
	<div class="fixed-nav-right">
		<ul>
			<?php if($ym) : ?>
			<li class="show-frame purple"><img src="<?php echo base_url(); ?>public/images/socmed/ym-on.png"></li>
			<?php else : ?>
			<li class="show-frame purple"><img src="<?php echo base_url(); ?>public/images/socmed/ym-off.png"></li>
			<?php endif; ?>

			<li class="show-frame blue"><img src="<?php echo base_url(); ?>public/images/socmed/facebook.png"></li>
			<li class="show-frame skyblue"><img src="<?php echo base_url(); ?>public/images/socmed/twitter.png"></li>
			<li class="show-frame red"><img src="<?php echo base_url(); ?>public/images/socmed/youtube.png"></li>
		</ul>


	</div>
	<div class="social-streamer" alt="">
		<ul>
			<li class="frame-content ym"><?php echo $ym_tag; ?></li>
			<li class="frame-content fb">fb</li>
			<li class="frame-content tw">tw</li>
			<li class="frame-content yt">
				<a href="http://www.youtube.com/watch?v=tbifxbNfY1s">
					<img src="<?php echo base_url(); ?>public/images/youtube.jpg">
				</a>
			</li>
		</ul>
	</div>
	<div class="wrapper">
		<div class="full">

			<div class="content" style="position:relative;">	
				<section class="clearfix" style="position:absolute;top:10px;right:50px">
					<div class="float-right chartbox"><span class="fa fa-shopping-cart fa-lg" style="margin-right:8px"></span>0 items</div>
				</section>
				
				<section class="clearfix headpanel">
					<div class="float-left">
							<div>
								<div>
									<a href="<?php echo base_url(); ?>">
										<img class="float-left" src="<?php echo base_url(); ?>public/images/logo-2.png"/>
									</a>
								</div>
								
								
							</div>
					</div>

					
				</section>
				

				

				<div>
					<div style="text-align:center">
						<ul style="margin:0 !important;display:inline-block !important;float:none !important" class="sf-menu" id="example" data-breakpoint="800">
							<li><a href="<?php echo base_url(); ?>">Home</a></li>
							<li>
								<a href="javascript:void(0)">Haji dan Umroh</a>
								<ul>
									<li><a href="javascript:void(0)">Paket Umroh</a>
										<ul class="child">
											<li><a href="<?php echo base_url() . $slug['umrah']['about']; ?>">Tentang Umroh</a></li>
											<li><a href="<?php echo base_url() . $slug['umrah']['biaya']; ?>">Paket Umroh</a></li>
											<li><a href="<?php echo base_url() . $slug['umrah']['persyaratan']; ?>">Persyaratan dan Ketentuan</a></li>
										</ul>
									</li>

							        <li><a href="javascript:void(0)">Paket Haji</a>
							        	<ul class="child">
											<li><a href="<?php echo base_url() . $slug['haji']['about']; ?>">Tentang Haji</a></li>
											<li><a href="<?php echo base_url() . $slug['haji']['biaya']; ?>">Paket Haji</a></li>
											<li><a href="<?php echo base_url() . $slug['haji']['persyaratan']; ?>">Persyaratan dan Ketentuan</a></li>
										</ul>
							        </li>
							        <li><a href="<?php echo base_url(); ?>berita/">Berita</a></li>
							        <li><a href="<?php echo base_url(); ?>gallery/">Gallery</a></li>
							    </ul>
							</li>
							<li>
								<a href="javascript:void(0)">Paket Tours Domestik & Luar Negri</a>
								<ul>
							        <li><a href="<?php echo base_url(); ?>domestik/">Domestik</a></li>
							        <li><a href="<?php echo base_url(); ?>luar-negri/">Luar Negri</a></li>
							        <li><a href="<?php echo base_url(); ?>rental/">Car Rental / Bus</a></li>
						        </ul>
							</li>
							<li>
								<a href="javascript:void(0)">Pengiriman Paket</a>
								<ul>
					                <li><a href="<?php echo base_url(); ?>layanan-antar-jemput/">Layanan Antar Jemput</a></li>
					                <li><a target="_blank" href="http://www.wahana.com/rate.html">Cek Biaya Antar</a></li>
					                <li><a href="<?php echo base_url(); ?>lokasi/">Kantor Kami (Posisi)</a></li>
				              	</ul>
							</li>
							<li>
								<a href="javascript:void(0)">Pemesanan Tiket</a>
								<ul>
					                <li><a target="_blank" href="http://klikmbc.co.id/">Cek Harga dan Tujuan</a></li>
					                <li><a href="<?php echo base_url(); ?>prosedur-pemesanan/">Prosedur Pemesanan via Telp</a></li>
				              	</ul>
							</li>
							<li>
								<a href="javascript:void(0)">AMC</a>
								<ul>
									<?php if(!empty($categories)) : ?>
						                <?php foreach($categories as $c) : ?>
						                <li><a href="<?php echo base_url(); ?>product/category/<?php echo $c->category_url_name; ?>/"><?php echo $c->category_display_name; ?></a></li>
						            	<?php endforeach; ?>
					            	<?php else : ?>
					            	<li><a href="<?php echo base_url(); ?>product/category/mukena/">Mukena</a></li>
					                <li><a href="<?php echo base_url(); ?>product/category/pakaian-ihrom/">Pakaian Ihrom</a></li>
					                <li><a href="<?php echo base_url(); ?>product/category/accessories/">Accessories</a></li>
					                <li><a href="<?php echo base_url(); ?>product/category/oleh-oleh/">Oleh-oleh Haji dan Umroh</a></li>
					            	<?php endif; ?>
				              	</ul>
							</li>
							<li>
								<a href="#contactus-popup-display" class="contact-modal-open" rel="modal:open" id="contactus-popup">Contact</a>
							</li>
						</ul>
					</div>
				</div>

				


				<!--
				modal
				-->
				<div id="contactus-popup-display" style="display:none;width:800px;z-index:9999999999 !important" class="clearfix">
					<div style="padding:15px" >
						<div class="float-left" style="width:370px">
							<h2 style="color:#11B997">amisyatour.com</h2>
							<div style="margin:10px 0">
								"Amisya Tours & Travel adalah biro perjalanan Umrah dan ONH untuk umat muslim yang hendak menunaikan Umrah dan Haji. Kami juga melayani pemesanan Tiket Domestic dan International."
							</div>
							<h2>Get In Touch!</h2>
							<div>
								<div style="margin:8px 0"><i class="fa fa-home fa-lg" style="margin-right:5px;"></i>
									<div style="display:inline;">Komplek Ruko Pasar Modern Batu Nunggal</div>
									<div style="margin-left:26px">Blok RD 35 Bandung</div>
									<div style="margin-left:26px">Indonesia</div>
								</div>
								<div style="margin:8px 0"><i class="fa fa-envelope fa-lg" style="margin-right:5px;"></i>
									amisyatours@yahoo.com
								</div>
								<div style="margin:8px 0"><i class="fa fa-phone fa-lg" style="margin-right:5px;"></i>
									(022) 87526751
								</div>
							</div>
						</div>
						<div class="float-right" style="width:370px">
							<h2>Send Us Message!</h2>
							<div>
								<form id="contact-form" name="contact-form" action="<?php echo base_url(); ?>welcome/contact/post/" method="POST">
									<table style="width:100%">
										<tr>
											<td>Name<span class="red">*</span> : </td>
											<td><input type="text" name="name" id="name" ></td>
										</tr>
										<tr>
											<td>Email<span class="red">*</span> : </td>
											<td><input type="text" name="email" id="email" ></td>
										</tr>
										<tr>
											<td>Message<span class="red">*</span> : </td>
											<td><textarea style="width: 273px; height: 123px;" type="text" name="message" id="message" ></textarea></td>
										</tr>
									</table>
								
								
								<div class="clearfix">
									<div class="float-left">
										<div id="status"></div>
									</div>
									<div class="float-right">
										<a href="javascript:void(0)" style="color:#FFFFFF !important" id="contactnow" class="btn btn-primary"><i class="icon-envelope icon-white" style="margin-right:5px"></i>Send</a>
									</div>
								</div>
								</form>
							</div>
						</div>
					</div>
				</div>