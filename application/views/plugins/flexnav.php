	<script src="<?php echo base_url(); ?>public/js/hoverIntent.js"></script>
	<script src="<?php echo base_url(); ?>public/js/superfish.js"></script>
	<link href="<?php echo base_url(); ?>public/css/superfish.css" rel="stylesheet" type="text/css">


	<script>

	    (function($){ //create closure so we can safely use $ as alias for jQuery

	      $(document).ready(function(){
	        


	        var exampleOptions = {
	          speed: 'fast'
	        }
	        // initialise plugin
	        var example = $('#example').superfish(exampleOptions);

	        // buttons to demonstrate Superfish's public methods
	        $('.destroy').on('click', function(){
	          example.superfish('destroy');
	        });

	        $('.init').on('click', function(){
	          example.superfish(exampleOptions);
	        });

	        $('.open').on('click', function(){
	          example.children('li:first').superfish('show');
	        });

	        $('.close').on('click', function(){
	          example.children('li:first').superfish('hide');
	        });

	        $('ul.child').hide();
	      });

	    })(jQuery);


	</script>