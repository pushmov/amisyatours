<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>HOME - amisyatours</title>
	<link href="http://fonts.googleapis.com/css?family=Signika:400,300,600,700" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/css/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/css/bootstrap.css" rel="stylesheet" type="text/css">
	
	<script type='text/javascript' src='<?php echo base_url();?>public/js/jquery.min.js'></script>
	<script type='text/javascript' src='<?php echo base_url();?>public/js/bootstrap.js'></script>

	<link href="<?php echo base_url(); ?>public/images/favico.png" rel="shortcut icon" type="image/x-icon" />

	<link href="<?php echo base_url(); ?>public/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<?php $this->load->view('plugins/modal'); ?>
	<?php $this->load->view('plugins/form_plugin'); ?>
	
	<script src="<?php echo base_url(); ?>public/js/icheck.js"></script>
	<link href="<?php echo base_url(); ?>public/css/polaris/polaris.css" rel="stylesheet">
	<script>
			$(document).ready(function(){
			  $('input').iCheck({
			    checkboxClass: 'icheckbox_polaris',
			    radioClass: 'iradio_polaris',
			    increaseArea: '-10%' // optional
			  });
			});
		</script>