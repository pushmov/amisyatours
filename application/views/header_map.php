	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
	<script language="javascript" type="text/javascript">
		function initialize() {
			var myLatlng = new google.maps.LatLng(parseFloat(<?php echo $latitude; ?>),parseFloat(<?php echo $longitude; ?>));
			var mapOptions = {
				zoom: 16,
				center: myLatlng
			}
			var map = new google.maps.Map(document.getElementById('map'), mapOptions);

			var marker = new google.maps.Marker({
					position: myLatlng,
					map: map,
					title: 'Amisyatours'
			});
		}

		google.maps.event.addDomListener(window, 'load', initialize);
	</script>