<!DOCTYPE html>
<html lang="en">
  <head>
  	<title>Admin - amisyatours</title>
	
		<link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/favico.png" />
		<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>public/js/jquery-ui.js"></script>

			
		<link href="<?php echo base_url(); ?>public/css/reveal.css" rel="stylesheet" type="text/css">
		<script src="<?php echo base_url(); ?>public/js/jquery.reveal.js"></script>			
		<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/tipsy.css" />
		<script src="<?php echo base_url(); ?>public/js/jquery.tipsy.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>public/js/jquery.simplemodal.js"></script>
	
		<script>
			$(function() {
				$( ".datepicker" ).datepicker();
			});
		</script>
		<link href="http://fonts.googleapis.com/css?family=Signika:400,300,600,700" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url(); ?>public/css/admin.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url(); ?>public/css/bootstrap.css" rel="stylesheet" type="text/css">
		<script src="<?php echo base_url(); ?>public/js/bootstrap.js"></script>

		<script src="<?php echo base_url(); ?>public/js/icheck.js"></script>
		<link href="<?php echo base_url(); ?>public/css/polaris/polaris.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>public/css/font-awesome.min.css" rel="stylesheet" type="text/css">



		<?php $this->load->view('plugins/modal'); ?>

		<?php $this->load->view('plugins/form_plugin'); ?>
		<?php $this->load->view('plugins/tinymce'); ?>
		<?php $this->load->view('plugins/bxslider'); ?>
		<?php $this->load->view('plugins/prettyphoto'); ?>

		<script>
		var d = document;
		var safari = (navigator.userAgent.toLowerCase().indexOf('safari') != -1) ? true : false;
		var gebtn = function(parEl,child) { return parEl.getElementsByTagName(child); };
		onload = function() {
		    
		    var body = gebtn(d,'body')[0];
		    body.className = body.className && body.className != '' ? body.className + ' has-js' : 'has-js';
		    
		    if (!d.getElementById || !d.createTextNode) return;
		    var ls = gebtn(d,'label');
		    for (var i = 0; i < ls.length; i++) {
		        var l = ls[i];
		        if (l.className.indexOf('label_') == -1) continue;
		        var inp = gebtn(l,'input')[0];
		        if (l.className == 'label_check') {
		            l.className = (safari && inp.checked == true || inp.checked) ? 'label_check c_on' : 'label_check c_off';
		            l.onclick = check_it;
		        };
		        if (l.className == 'label_radio') {
		            l.className = (safari && inp.checked == true || inp.checked) ? 'label_radio r_on' : 'label_radio r_off';
		            l.onclick = turn_radio;
		        };
		    };
		};
		var check_it = function() {
		    var inp = gebtn(this,'input')[0];
		    if (this.className == 'label_check c_off' || (!safari && inp.checked)) {
		        this.className = 'label_check c_on';
		        if (safari) inp.click();
		    } else {
		        this.className = 'label_check c_off';
		        if (safari) inp.click();
		    };
		};
		var turn_radio = function() {
		    var inp = gebtn(this,'input')[0];
		    if (this.className == 'label_radio r_off' || inp.checked) {
		        var ls = gebtn(this.parentNode,'label');
		        for (var i = 0; i < ls.length; i++) {
		            var l = ls[i];
		            if (l.className.indexOf('label_radio') == -1)  continue;
		            l.className = 'label_radio r_off';
		        };
		        this.className = 'label_radio r_on';
		        if (safari) inp.click();
		    } else {
		        this.className = 'label_radio r_off';
		        if (safari) inp.click();
		    };
		};

		</script>
		<script>
			$(document).ready(function(){
			  $('input').iCheck({
			    checkboxClass: 'icheckbox_polaris',
			    radioClass: 'iradio_polaris',
			    increaseArea: '-10%' // optional
			  });
			});
		</script>
  </head>
  <body>