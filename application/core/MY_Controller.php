<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

	var $ym = 'irwan_telkom';
	var $email = 'irwan_telkom@yahoo.co.id';

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
		
	}

	protected function _ym(){
		$yahooid = $this->ym;
		$ch = curl_init("http://opi.yahoo.com/online?u=".$yahooid."&m=t");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$status = curl_exec($ch);
		curl_close($ch);
		if($status == $yahooid." is NOT ONLINE"){
			return false;
		} elseif ($status == $yahooid." is ONLINE"){
			return true;
		}
	}

	protected function _socmed(){
		$this->Appmodel->set_table('social_media');
		$result = $this->Appmodel->fetch_rows(NULL,array('status' => 1));
		return $result;
	}

	protected function _slug(){
		$this->Appmodel->set_table('app_routes');
		$routes = $this->Appmodel->fetch_rows();

		$slug = array();
		foreach($routes as $route){
			$slug[$route->control][$route->method] = $route->slug;
		}
		return $slug;
	}

	protected function _common_data($data){
		$data['slug'] = $this->_slug();
		$data['socmed'] = $this->_socmed();
		$data['ym'] = $this->_ym();
		$data['ym_tag'] = '<a href="ymsgr:sendIM?'.$this->email.'"><img src="http://opi.yahoo.com/online?u='.$this->ym.'&amp;m=g&amp;t=19" border="0" alt="" /></a>';

		$this->load->library('RecaptchaLib');
		$recaptcha = new RecaptchaLib();
		$data['recaptcha_form'] = $recaptcha->recaptcha_get_html(RECAPTCHA_PUBLIC_KEY);
		$data['categories'] = $this->_get_categories();
		
		return $data;
	}

	protected function _get_categories(){

		$this->Appmodel->set_table('categories');
		$data = $this->Appmodel->fetch_rows();

		return $data;
	}

	protected function convert_currency($amount){
		$this->load->library('ForeignExchange');
		$fx = new ForeignExchange();

		return number_format($fx->toForeign($amount) + 100, 2);


	}

	protected function is_ajax(){
		return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']=="XMLHttpRequest") ? true : false;
	}

	protected function paket($kode){
		$a = array(
			'eko' => 'Ekonomi',
			'reg' => 'Regular',
			'eks' => 'Eksekutif',
			'tur' => 'Umrah & Turkey'
		);

		return $a[$kode];
	}

}