<?php

Class Product_model extends Appmodel
{

	function get_products($size=NULL){
		if(isset($size)){
			$this->db->limit($size);
		}

		$this->db->order_by('date_created DESC');

		return $this->db->get($this->table)
		->result();

	}

	function get_products_page($offset=NULL,$limit=NULL){
		$this->db->order_by('date_created DESC');

		return $this->db->get($this->table,$limit, $offset)
		->result();

	}

	function products_by_category($where){
		return $this->db->where($where)
			->get($this->table)
			->result();

	}

}
