<?php

Class Appmodel extends CI_Model
{

	var $table = NULL;

	function set_table($table){
	
			$this->table = $table;
	}

	function update($data,$where=NULL){
	
			if(isset($where)){
			
					$this->db->where($where);
					
			}
			$this->db->update($this->table,$data);
	}
	
	function insert($data){
	
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
	}
	
	function delete($where){
	
			$this->db->where($where)
					->delete($this->table);
					
	}

	function fetch_rows($select=NULL,$where=NULL){

			if(isset($select)){
					$this->db->select($select);
			}

			if(isset($where)){
					$this->db->where($where);
			}

			return $this->db->get($this->table)
					->result();
	}

	function fetch_row($select=NULL,$where=NULL){

			if(isset($select)){
					$this->db->select($select);
			}

			if(isset($where)){
					$this->db->where($where);
			}

			return $this->db->get($this->table)
					->row();
	}

	function custom_rows($select=NULL,$where=NULL){

			if(isset($select)){
					$this->db->select($select);
			}

			if(isset($where)){
					$this->db->where($where,NULL,FALSE);
			}

			return $this->db->get($this->table)
					->result();
	}

	function custom_row($select=NULL,$where=NULL){

			if(isset($select)){
					$this->db->select($select);
			}

			if(isset($where)){
					$this->db->where($where);
			}

			return $this->db->get($this->table)
					->row();
	}

	function all_desc($select=NULL,$where=NULL,$offset=NULL,$limit=NULL){

		if(isset($select)){
			$this->db->select($select);
		}

		if(isset($where)){
			$this->db->where($where);
		}

		$this->db->order_by('date_created DESC');

		return $this->db->get($this->table,$limit, $offset)->result();

	}

}