	<?php $this->load->view('header'); ?>
	<?php $this->load->view('plugins/camera'); ?>
	<?php $this->load->view('plugins/flexnav'); ?>
	<?php $this->load->view('plugins/newsticker'); ?>
	<?php $this->load->view('header_end'); ?>
	<?php $this->load->view('statics/menu'); ?>
	<?php $this->load->view('statics/slider'); ?>
				


				<div class="content" style="padding: 10px 0 !important" style="margin-bottom:40px">
					<div style="margin:20px 0" class="left fs24"><span style="color:#2C9192">Special</span> Offers :</div>
					<table align="center">
							<tr>
								<?php $counter = 1; ?>
								<?php foreach($products as $product) : ?>
								<td style="padding:0">
									<div class="product-wrapper" style="text-align:center">
										<div style="display:none;position:absolute;bottom:0;padding:20px;">
											<?php echo $product->name; ?>
										</div>
										<a href="<?php echo base_url(); ?>product/detail/<?php echo $product->links; ?>">
											<img src="<?php echo base_url(); ?>public/images/products/<?php echo $product->product_gallery; ?>" width="160" height="160">
										</a>
									</div>
								</td>
								<?php if($counter % 6 == 0) : ?></tr><tr><?php endif; ?>
								<?php $counter++; ?>
								<?php endforeach; ?>
								
							</tr>
					</table>
				</div>
				<div>
					<div style="margin:20px 0" class="left fs24"><span style="color:#2C9192">Berita</span> Terbaru:</div>
					<div>
						<table style="width:100%" align="center">
							<?php $counter = 1; ?>
							<tr>
							<?php foreach($news as $new) : ?>
								<?php if($counter == 9) break; ?>

								

								<td>
									<div class="berita-wrapper" style="margin:9px 5px;">
										<div class="clearfix">
											<div class="float-left" style="margin-right:10px;">
												<img src="<?php echo base_url(); ?>public/images/berita/<?php echo $new->gallery; ?>" width="160" height="120" style="height:110px;">
											</div>
											<div class="float-right" style="min-width:50px;margin-right:10px">
												<div style="text-align:center">
													<h3 style="color:#FFFFFF;margin-bottom:0;background:#13AD85;"><?php echo strtoupper(date('M',strtotime($new->date_created)));?></h3>
													<h1 style="margin:0;border:1px solid #c9c9c9"><?php echo date('d',strtotime($new->date_created)); ?></h1>
												</div>
												
											</div>
										</div>
										<div>
											<h5><?php echo $new->title; ?></h5>

											<?php
											$string = strip_tags($new->content);

											if (strlen($string) > 100) {

											    // truncate string
											    $stringCut = substr($string, 0, 100);

											    // make sure it ends in a word so assassinate doesn't become ass...
											    $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
											}
											 echo $string; ?>
										</div>
										<div style="text-align:right"><a href="<?php echo base_url(); ?>berita/detail/<?php echo $new->link; ?>">readmore</a></div>
									</div>
									
								</td>
								<?php if($counter % 3 == 0) : ?>
								</tr><tr>
								<?php endif; ?>

								
								<?php $counter++; ?>
								<?php endforeach; ?>
							</tr>
						</table>
					</div>
				</div>
				
				</section>
			</div>
				<?php //$this->load->view('statics/news'); ?>
				<?php $this->load->view('statics/footer'); ?>
		</div>
		

	<?php $this->load->view('html_end'); ?>