	<?php $this->load->view('header'); ?>

	<?php $this->load->view('header_end'); ?>
<body>
	<div class="wrapper center" style="margin:100px auto">

		<div style="padding:40px 0;background:#FFFFFF;height:280px">
			<h3>Administrator Panel</h3>
			<form action="<?php echo base_url(); ?>welcome/admin_login/" method="POST" id="admin-form" name="admin-login">
					<table style="width:40%;margin:auto" align="center">
						<tr>
							<td>Username :</td>
							<td><input type="text" name="username" id="username" placeholder="Username" ></td>
						</tr>
						<tr>
							<td>Password :</td>
							<td><input type="password" name="password" id="password" placeholder="Password"></td>
						</tr>

						<tr>
							<td colspan="2" class="right" style="padding:0 80px">
								<a id="submit" href="javascript:void(0)" class="btn btn-sm btn-primary" style="color:#FFFFFF !important"><span class="glyphicon icon-user icon-white"></span> Login</a>
							</td>
						</tr>
						<?php if($this->session->flashdata('error')) : ?>
						<tr>
							<td colspan="2">
								<div style="margin:10px 0">
									  <div class="alert alert-error">
							    		<button type="button" class="close" data-dismiss="alert">&times;</button>
							    		<?php echo $this->session->flashdata('error'); ?>
							    	</div>
								</div>
							</td>
						</tr>
						<?php endif; ?>

						<?php if($this->session->flashdata('success')) : ?>
						<tr>
							<td colspan="2">
								<div style="margin:10px 0">
									  <div class="alert alert-success">
							    		<button type="button" class="close" data-dismiss="alert">&times;</button>
							    		<?php echo $this->session->flashdata('success'); ?>
							    	</div>
								</div>
							</td>
						</tr>
						<?php endif; ?>
					</table>
			</form>
		</div>
		<script>
			$('#submit').click(function(){
				if($('#username').val() == ''){
					return false;
				}
				if($('#password').val() == ''){
					return false;
				}
				$('#admin-form').submit();
			});
		</script>

	<?php $this->load->view('html_end'); ?>