<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

	const DEFAULT_PRODUCTS = 8;

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
		$this->load->model('Product_model');
	}
	
	public function index()
	{

		$this->Appmodel->set_table('slides');
		$data['sliders'] = $this->Appmodel->fetch_rows();

		$this->Appmodel->set_table('options');
		$product_size = $this->Appmodel->fetch_row(NULL,array('name' => 'product'));
		
		if(!empty($product_size)){

			$arrsize = json_decode($product_size->value);
			$size = $arrsize->size;

		} else {
			$size = self::DEFAULT_PRODUCTS;
		}

		$this->Product_model->set_table('products');
		$result = $this->Product_model->get_products($size);

		$data['products'] = $result;

		$this->Product_model->set_table('news');
		$news = $this->Product_model->get_products(9);
		$data['news'] = $news;

		$data = $this->_common_data($data);
		$this->load->view('welcome_message',$data);
	}

	public function admin(){
		$this->load->view('welcome/admin_index');
	}

	public function admin_login(){
		
		if(!$this->input->post('username') || !$this->input->post('password')){
			$this->session->set_flashdata('error','<strong>Erorr!</strong> Invalid username or password');
			redirect('welcome/admin');
		}

		$this->Appmodel->set_table('users');
		$username = $this->input->post('username');
		$password = $this->input->post('password');


		$row = $this->Appmodel->fetch_row(NULL,array('username' => $username, 'hashed_password' => sha1($password), 'is_active' => 1));
		if(empty($row)){
			$this->session->set_flashdata('error','<strong>Erorr!</strong> Invalid username or password');
			redirect('welcome/admin');
		}

		$session = array(
				'id' => $row->id,
				'username' => $row->username,
				'code' => $row->activation_code
			);
		$this->session->set_userdata('admin_amisyatours',$session);
		redirect('dashboard/admin/index');

	}

	public function admin_logout(){
		$this->session->unset_userdata('admin_amisyatours');
		session_unset();
		session_destroy();
		
		$this->session->sess_destroy();
		$this->session->set_flashdata('success','<strong>Success</strong> You successfully logout.');
		redirect('welcome/admin');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */