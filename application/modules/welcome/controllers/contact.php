<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
	}

	public function index(){
		if($this->session->flashdata('email')){
			$data = array();
			$data = $this->_common_data($data);
			$this->load->view('register_success',$data);
		}
	}

	private function _sendemail($params){
		$send_to = 'amisyatours@yahoo.com';
		// subject
		$subject = (isset($params['subject'])) ? $params['subject'] : 'Message from ' . $params['name'];

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		$headers .= 'From: Visitor <'.$params['from'].'>' . "\r\n";

		// Mail it
		mail($send_to, $subject, $params['message'], $headers);
		mail('irwan_telkom@yahoo.co.id', $subject, $params['message'], $headers);
	}

	public function post(){

		$response = array();
		
		$this->load->library('RecaptchaLib');
		$recaptcha = new RecaptchaLib();

		if(!$this->input->post('name') || !$this->input->post('email') || !$this->input->post('message')){
			$response['message'] = 'Please enter all fields.';
			echo json_encode($response);	
			exit();
		}

		$request = array(
			'message_text' => $this->input->post('message'),
			'email' => $this->input->post('email'),
			'name' => $this->input->post('name')
		);

		$this->Appmodel->set_table('messages');
		$this->Appmodel->insert($request);

		if(ENVIRONMENT == 'production'){
			$params = array(
				'name' => $request['name'],
				'from' => $request['email'],
				'message' => $request['message_text']
			);
			$this->_sendemail($params);	
		}

		$response['message'] = 'Thank you for contacting us.';
		echo json_encode($response);	
		exit();

	}

	public function submit_jamaah(){
		
		$jk = $this->input->post('jk');
		$nama = $this->input->post('nama');
		$passport = $this->input->post('passport');
		$dob = $this->input->post('dob');
		$this->Appmodel->set_table('form_activity');
		$this->Appmodel->insert(array('activity' => serialize($_POST)));


		$body = '<table style="width:60%;"><tr>
		<td colspan="4">Data Jamaah Haji - submitted at : '.date('D, d/m/Y H:i:s').'</td>
		<tr>
		<td style="font-weight:bolder">Nomer</td>
		<td style="font-weight:bolder">Nama</td>
		<td style="font-weight:bolder">Gender</td>
		<td style="font-weight:bolder">Tanggal Lahir</td>
		<td style="font-weight:bolder">No. Passpor</td>
		</tr>';


		//send email to support
		

		$counter = 1;
		foreach($jk as $j => $k){
			$body .= '<tr>
			<td>'.$counter.'</td>
			<td>'.$nama[ $counter ].'</td>
			<td>'.$jk[ $counter ].'</td>
			<td>'.$dob[ $counter ].'</td>
			<td>'.$passport[ $counter ].'</td>
			</tr>';
			$counter++;
		}

		if(ENVIRONMENT == 'production'){
			$emails = array(
				'subject' => count($jk) . ' Jamaah Umrah',
				'from' => $this->input->post('email'),
				'message' => $body
			);
			$this->_sendemail($params);
		}
		$this->session->set_flashdata('email',true);
		redirect('welcome/contact/');

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */