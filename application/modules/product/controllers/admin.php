<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller {

	const PRODUCT_PER_PAGE = 5;
	const MAX_ALLOWED_SHIPMENT_PICTURE = 5120;
	const FILE_ALLOWED_TYPES = 'jpg|jpeg|bmp|gif|png|JPG|JPEG|BMP|GIF|PNG';

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
		$this->load->model('Product_model');
		$this->Product_model->set_table('products');
	}

	public function index($page=0){
		
		$all = $this->Product_model->get_products_page();

		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'product/admin/index/';
		$config['total_rows'] = count($all);
		$config['per_page'] = self::PRODUCT_PER_PAGE;
		$config['cur_tag_open'] = '<span class="yellow" style="margin:0 10px">';
		$config['cur_tag_close'] = '</span>';
		$config['next_page'] = '&laquo;';
		$config['prev_page'] = '&raquo;';

		$result = $this->Product_model->get_products_page($page,self::PRODUCT_PER_PAGE);

		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();

		$data['result'] = $result;
		$this->load->view('admin_product_index',$data);
	}

	public function edit($id=NULL){
		if(!isset($id)){
			redirect('product/admin/index');
		}

		$row = $this->Product_model->fetch_row(NULL,array('id' => $id));
		if(empty($row)){
			redirect('product/admin/index');
		}

		$data['categories'] = $this->_get_categories();

		$data['detail'] = $row;
		$this->load->view('admin_product_edit',$data);

	}

	public function add(){
		$data['categories'] = $this->_get_categories();
		$this->load->view('admin_product_add',$data);
	}

	public function addcategory(){
		$name = $this->input->post('category');
		$url_name = strtolower(str_replace(' ', '-', $name));

		$request = array(
			'category_display_name' => $name,
			'category_url_name' => $url_name
		);

		$this->Product_model->set_table('categories');
		$this->Product_model->insert($request);
		redirect('/product/admin/index');
	}

	public function save(){

		if(!$this->input->post('name')){
			redirect('product/admin/index/');
		}

		if($this->input->post('id')){
			$id = $this->input->post('id');
			$detail = $this->Product_model->fetch_row(NULL,array('id' => $id));
			$photo1 = $detail->product_gallery;
		} else {
			$photo1 = '';
		}

		$name = (!empty($_FILES['gallery']['tmp_name'])) ? time() : '';

		if($name != ''){

			$config['allowed_types'] = self::FILE_ALLOWED_TYPES;
			$config['max_size'] = self::MAX_ALLOWED_SHIPMENT_PICTURE;
			$config['upload_path'] = './public/images/products/';
			$config['file_name'] = $name;
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('gallery'))
			{
				echo $this->upload->display_errors();
				exit();
			}
			else
			{
				$upload_data = $this->upload->data();
				$photo1 = $upload_data['orig_name'];
			}

		}
			

		$request = array(
			'name' => $this->input->post('name'),
			'description' => $this->input->post('desc'),
			'posted_by' => 1,
			'price' => $this->input->post('price'),
			'product_gallery' => $photo1,
			'category_id' => $this->input->post('category')
		);

		if($this->input->post('id'))
		{
			$id = $this->input->post('id');
			$where = array('id' => $id);
			$request['date_updated'] = date('Y-m-d H:i:s');
			$this->Product_model->update($request, $where);

		} else {

			$id = $this->Product_model->insert($request);
			$links = date('Y/m/d') . '/'.strtolower(str_replace(' ', '-', $this->input->post('name'))).'/'.$id.'/';

			$update_photo = array(
				'product_gallery' => $photo1,
				'links' => $links
			);

			$this->Product_model->update($update_photo, array('id' => $id));
		}

		redirect('product/admin/index');

	}

	public function delete($id=NULL){
		if(!isset($id)){
			redirect('product/admin/index/');
		}


		$this->Product_model->delete(array('id' => $id));

		$this->Product_model->set_table('products_gallery');
		
		foreach($this->Product_model->fetch_rows(NULL,array('product_id' => $id)) as $k => $r){
			$this->Product_model->delete(array('gallery_id' => $r->gallery_id));
			unlink('./public/images/products/'.$r->filename);
		}

		$this->session->set_flashdata('success', 'Product deleted');
		redirect('product/admin/index/');
	}

	public function picture($id=NULL){
		if(!isset($id)){
			redirect('product/admin/index');
		}

		$this->Product_model->update(array('product_gallery' => ''), array('id' => $id));
		redirect('product/admin/edit/'.$id);
	}

	public function gallery($id=NULL){
		if(!isset($id)){
			redirect('product/admin/index');
		}

		
		$this->Product_model->set_table('products');
		$data['detail'] = $this->Product_model->fetch_row(NULL,array('id' => $id));

		$this->Product_model->set_table('products_gallery');
		$data['gallery'] = $this->Product_model->fetch_rows(NULL,array('product_id' => $id));

		$this->load->view('admin_product_gallery',$data);
	}

	public function gallery_xhr($id=NULL){

			$name = (!empty($_FILES['file']['tmp_name'])) ? time() : '';

			$config['allowed_types'] = self::FILE_ALLOWED_TYPES;
			$config['max_size'] = self::MAX_ALLOWED_SHIPMENT_PICTURE;
			$config['upload_path'] = './public/images/products/';
			$config['file_name'] = $name;
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('file'))
			{
				print_r($this->upload->data());
				exit();
			}
			else
			{
				$upload_data = $this->upload->data();
				$photo1 = $upload_data['orig_name'];
			}

			$newdata = array(
				'product_id' => $id,
				'filename' => $photo1
			);
			$this->Product_model->set_table('products_gallery');
			$this->Product_model->insert($newdata);

			$response = array(
				'success' => 'success',
			);

			echo json_encode($response);
			exit();

	}

}