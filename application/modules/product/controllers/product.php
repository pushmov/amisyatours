<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
		$this->load->model('Product_model');
		$this->Product_model->set_table('products');
	}

	public function index(){
		
	}

	public function detail($year=NULL,$month=NULL,$day=NULL,$name=NULL,$id=NULL){

		if(!isset($year) || !isset($month) || !isset($day) || !isset($name) || !isset($id)){
			redirect('/');
		}

		$params = $year . '/' . $month . '/' .$day. '/' . $name . '/' . $id . '/';
		$row = $this->Product_model->fetch_row(NULL,array('id' => $id));
		if(empty($row)){
			redirect('/');
		}

		if($row->links != $params){
			redirect('/');
		}

		$data['detail'] = $row;

		$this->Product_model->set_table('products_gallery');
		$data['gallery'] = $this->Product_model->fetch_rows(NULL,array('product_id' => $id));

		$data = $this->_common_data($data);
		$this->load->view('product_detail',$data);
	}

	public function category($slug=NULL){
		if(!isset($slug)){
			redirect('/');
		}

		$this->Appmodel->set_table('categories');
		$rows_category = $this->Appmodel->fetch_row(NULL,array('category_url_name' => $slug));

		if(empty($rows_category)){
			redirect('/');
		}

		$category_id = $rows_category->category_id;

		$this->Product_model->set_table('products');
		$rows_product = $this->Product_model->products_by_category(array('category_id' => $category_id));

		$data['products'] = $rows_product;
		$data['product_name'] = $rows_category->category_display_name;
		$data = $this->_common_data($data);

		$this->load->view('product_category',$data);


	}

}