	<?php $this->load->view('header'); ?>
	<?php $this->load->view('plugins/flexnav'); ?>
	<?php $this->load->view('plugins/newsticker'); ?>
	<?php $this->load->view('header_end'); ?>
	<?php $this->load->view('statics/menu'); ?>
				


				<div class="content" style="padding: 10px 0 !important" style="margin-bottom:40px">
					<div style="margin:20px 0" class="left fs24"><span style="color:#2C9192">Category</span> <?php echo $product_name; ?> :</div>
					<h4><?php echo sizeof($products); ?> products found</h4>

					<div style="margin:20px 0">
					<?php if(!empty($products)) : ?>

					<table align="center">
							<tr>
								<?php $counter = 1; ?>
								<?php foreach($products as $product) : ?>
								<td style="padding:0;margin:15px;">
									<div class="product-wrapper" style="text-align:center;">
										<div style="display:none;position:absolute;bottom:0;padding:20px;">
											<?php echo $product->name; ?>
										</div>
										<a href="<?php echo base_url(); ?>product/detail/<?php echo $product->links; ?>">
											<img src="<?php echo base_url(); ?>public/images/products/<?php echo $product->product_gallery; ?>" width="160" height="160">
										</a>


									</div>
									
								</td>
								<?php if($counter % 6 == 0) : ?></tr><tr><?php endif; ?>
								<?php $counter++; ?>
								<?php endforeach; ?>
								
							</tr>
					</table>
					<?php else :?>
					No records found
					<?php endif; ?>
					</div>
				</div>
				
				
				</section>
			</div>
				<?php //$this->load->view('statics/news'); ?>
				<?php $this->load->view('statics/footer'); ?>
		</div>
		

	<?php $this->load->view('html_end'); ?>