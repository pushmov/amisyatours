<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<h2>Create new Product</h2>

			<div id="ajax-response">
				<form enctype="multipart/form-data" name="create-product" id="create-product" method="POST" action="<?php echo base_url(); ?>product/admin/save/">
				<table class="maintable" style="width:100%;border-top:4px solid #71C39A">
					<tr>
						<td>Category:</td>
						<td>
							<select name="category">
								<?php foreach($categories as $c) : ?>
								<option value="<?php echo $c->category_id; ?>"><?php echo $c->category_display_name; ?></option>
								<?php endforeach; ?>
							</select>
							<div>
								<a rel="modal:open" href="#ex1" class="pointer" id="add-category"><i class="icon-plus"></i>Add new category</a>
							</div>
						</td>
					</tr>
					<tr>
						<td>Display Picture (160x160 px)<span class="red">*</span></td>
						<td><input type="file" name="gallery" id="gallery" ></td>
					</tr>

					<tr>
						<td>Name<span class="red">*</span></td>
						<td><input style="width:80%" type="text" name="name" id="name" ></td>
					</tr>
					<tr>
						<td>Description<span class="red">*</span></td>
						<td><textarea name="desc" id="desc" style="width:100%;height:358px;"></textarea></td>
					</tr>
					<tr>
						<td>Price<span class="red">*</span></td>
						<td><input type="text" name="price" id="price" ></td>
					</tr>
					
					<tr>
						<td colspan="2" class="right"><button class="btn btn-primary"><i class="icon-ok icon-white"></i>Create</button></td>
					</tr>
				</table>
				</form>
				<div>Field dengan mark (<span class="red">*</span>) wajib diisi</div>
			</div>
		</div>

</div>
<div id="ex1" style="display:none;">
	<h4>Add Category</h4>
	<form id="uploadform" name="uploadform" action="<?php echo base_url(); ?>product/admin/addcategory/" method="POST">
		<table class="maintable" style="border-top:none;">
			<tr>
				<td>Name : </td>
				<td>
					<div style="margin:10px 0"><input type="text" name="category" id="category"></div>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="right">
					<button class="btn btn-primary" type="submit" id="add"><i style="margin-right:5px;" class="icon-upload icon-white"></i>Add</button>
				</td>
			</tr>
		</table>
	</form>
</div>
<script>
	function deleteUser(id){
		var c = confirm('Are you sure want to delete?');
		if(c){
			window.location="<?php echo base_url(); ?>user/admin/delete/"+id;
		}
		return c;
	}
</script>
<?php $this->load->view('admin_footer'); ?>
