	<?php $this->load->view('header'); ?>
	<?php $this->load->view('plugins/flexnav'); ?>
	<?php $this->load->view('plugins/sharethis'); ?>
	<?php $this->load->view('plugins/bxslider'); ?>
	<?php $this->load->view('plugins/prettyphoto'); ?>
	<?php $this->load->view('header_end'); ?>
	<?php $this->load->view('statics/menu'); ?>

				<div class="content" style="padding: 10px 0 !important">
					<div style="padding:25px">
						<div class="left fs24" style="margin:10px 0"><span style="color:#2C9192">Product Description : </span><?php echo $detail->name; ?></div>
						<?php if($detail->date_updated != '') :?>
						<p style="font-weight:400;font-family:PTSans-regular !important">Last Update : <?php echo date('D, d/M/Y H:i:s',strtotime($detail->date_updated)); ?></p>
						<?php endif; ?>

						<div class="clearfix" style="margin:20px 0">
							<div class="float-left" style="margin-right:20px;width:35%">
								<div class="product-gallery-wrapper center">
									<div class="gallery">
											<ul class="bxslider" style="height:400px">
												<li>
													<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/products/<?php echo $detail->product_gallery; ?>">
														<img src="<?php echo base_url(); ?>public/images/products/<?php echo $detail->product_gallery; ?>" width="200">
													</a>
												</li>

												<?php foreach($gallery as $g) : ?>
												<li>
													<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/products/<?php echo $g->filename; ?>">
														<img src="<?php echo base_url(); ?>public/images/products/<?php echo $g->filename; ?>">
													</a>
												</li>
												<?php endforeach; ?>
											</ul>
									</div>

									<div>Harga : <?php echo $detail->price; ?></div>
									<div style="margin:8px 0"><button class="btn btn-primary"><i class="fa fa-shopping-cart fa-lg" style="margin-right:5px"></i>Add to cart</button></div>
								</div>

							</div>
							<div>
								
								<div style="font-family:PTSans-regular !important;padding:0 15px">
									<?php echo $detail->description; ?>
								</div>
							</div>
						</div>

						<!--gallery box-->
						

						
						<div class="center">
							<p style="font-weight:900 !important;font-family:PTSans-regular !important;font-size:18px">Share This :</p>
							<div class="" style="margin:10px 0">
								<span class='st_sharethis_large' displayText='ShareThis'></span>
								<span class='st_facebook_large' displayText='Facebook'></span>
								<span class='st_twitter_large' displayText='Tweet'></span>
								<span class='st_googleplus_large' displayText='Google +'></span>
								<span class='st_linkedin_large' displayText='LinkedIn'></span>
								<span class='st_pinterest_large' displayText='Pinterest'></span>
							</div>
						</div>
					</div>
				</div>
				</section>
			</div>
				<?php $this->load->view('statics/footer'); ?>
		</div>
	<script>

		$(document).ready(function(){
		  $('.bxslider').bxSlider({
		  	adaptiveHeight: false,
		  	responsive: true,
  			mode: 'fade',
  			slideWidth: 500,
		    minSlides: 2,
		    maxSlides: 3,
		    slideMargin: 10
		  });
		});
	</script>

	<?php $this->load->view('html_end'); ?>