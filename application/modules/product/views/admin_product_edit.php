<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<div class="clearfix">
				<div class="float-left"><h2>Edit Product</h2></div>
				<div class="float-right"><button class="btn btn-primary" onclick="window.location='<?php echo base_url(); ?>product/admin/gallery/<?php echo $detail->id; ?>'" title="Add more picture to this product. Pictures will be displayed as slide gallery in product detail."><i class="icon-picture icon-white"></i>Add more pictures</button></div>
			</div>

			<div id="ajax-response">
				<form name="edit-product" id="edit-product" method="POST" action="<?php echo base_url(); ?>product/admin/save/" enctype="multipart/form-data">
				<input type="hidden" name="id" id="id" value="<?php echo $detail->id; ?>" >
				<table class="maintable" style="width:100%;border-top:4px solid #71C39A">
					<tr>
						<td>Category:</td>
						<td>
							<select name="category" id="category">
								<?php foreach($categories as $c) : ?>
								<option value="<?php echo $c->category_id; ?>"><?php echo $c->category_display_name; ?></option>
								<?php endforeach; ?>
							</select>
							<div>
								<a rel="modal:open" href="#ex1" class="pointer" id="add-category"><i class="icon-plus"></i>Add new category</a>
							</div>
						</td>
					</tr>

					<tr>
						<td>Display Picture<span class="red">*</span> 160x160px</td>
						<td>
							<?php if($detail->product_gallery != '') : ?>
							<img src="<?php echo base_url(); ?>public/images/products/<?php echo $detail->product_gallery; ?>">
							<div style="margin:10px 0"><a style="color:#000000 !important" class="btn" href="<?php echo base_url(); ?>product/admin/picture/<?php echo $detail->id; ?>"><i class="icon-refresh"></i>Change Pic</a></div>
							<?php else : ?>
							<input type="file" name="gallery" id="gallery" >
							<?php endif; ?>
						</td>
					</tr>
					<tr>
						<td>Name<span class="red">*</span></td>
						<td><input style="width:80%" type="text" name="name" id="name" value="<?php echo $detail->name; ?>"></td>
					</tr>
					<tr>
						<td>Description<span class="red">*</span></td>
						<td><textarea name="desc" id="desc" style="width:100%;height:358px;"><?php echo $detail->description; ?></textarea></td>
					</tr>
					<tr>
						<td>Price<span class="red">*</span></td>
						<td><input type="text" name="price" id="price" value="<?php echo $detail->price; ?>"></td>
					</tr>
					
					<tr>
						<td colspan="2" class="right"><button class="btn btn-primary"><i class="icon-ok icon-white"></i>Create</button></td>
					</tr>
				</table>
				</form>
				<div>Field dengan mark (<span class="red">*</span>) wajib diisi</div>
			</div>
		</div>

</div>

<div id="ex1" style="display:none;">
	<h4>Add Category</h4>
	<form id="uploadform" name="uploadform" action="<?php echo base_url(); ?>product/admin/addcategory/" method="POST">
		<table class="maintable" style="border-top:none;">
			<tr>
				<td>Name : </td>
				<td>
					<div style="margin:10px 0"><input type="text" name="category" id="category"></div>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="right">
					<button class="btn btn-primary" type="submit" id="add"><i style="margin-right:5px;" class="icon-upload icon-white"></i>Add</button>
				</td>
			</tr>
		</table>
	</form>
</div>

<script>
	function deleteUser(id){
		var c = confirm('Are you sure want to delete?');
		if(c){
			window.location="<?php echo base_url(); ?>user/admin/delete/"+id;
		}
		return c;
	}

	$(document).ready(function(){
		$('#category').val('<?php echo $detail->category_id; ?>');
	});
</script>
<?php $this->load->view('admin_footer'); ?>
