<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<div class="clearfix">
				<div class="float-left"><h2>Add gallery to product id : <?php echo $detail->id; ?></h2></div>
			</div>

			<div id="ajax-response">
				<form name="f-upload" id="f-upload" action="<?php echo base_url(); ?>product/admin/gallery_xhr/<?php echo $detail->id; ?>" method="POST" >
				
				<table class="maintable" style="width:100%;border-top:4px solid #71C39A">
					
					<tr>
						<td>Product Name: </td>
						<td>
							<div><?php echo $detail->name; ?></div>
						</td>
					</tr>

					<tr>
						<td>Current pictures : </td>
						<td>
							<div><?php echo sizeof($gallery); ?></div>
						</td>
					</tr>

					<?php if(sizeof($gallery) > 0) :?>
					<tr>
						<td colspan="2" >
							User display :
							<div class="gallery center" style="width:32%;">
								<ul style="display:inline-block" class="bxslider">
									<?php foreach ($gallery as $g) : ?>
									<li>
										<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/products/<?php echo $g->filename; ?>">
											<img src="<?php echo base_url(); ?>public/images/products/<?php echo $g->filename; ?>" width="200">
										</a>
									</li>
									<?php endforeach; ?>
								</ul>
							</div>
						</td>
					</tr>
					<?php endif; ?>

					<tr>
						<td>Choose picture</td>
						<td>
							<div>
								<input type="file" name="file" id="file" value="">
							</div>
							<div>
								<div id="notifier" style="display:none">Uploading ...</div>
								<div id="progress-1" class="progress">
									<div class="bar"><span class="percent"></span></div >
								</div>
							</div>
						</td>
					</tr>
					
					<tr>
						<td colspan="2" class="right">
							<a style="color:#000000 !important" class="btn" href="<?php echo base_url(); ?>product/admin/edit/<?php echo $detail->id; ?>"><i class="icon-edit"></i>Back to edit page</button>
							<a style="margin-left:8px;color:#FFFFFF !important" href="javascript:void(0)" id="upload" class="btn btn-primary"><i class="icon-upload icon-white"></i>Upload</a>
						</td>
					</tr>
				</table>
				</form>
			</div>
		</div>

</div>
<script>

	$('#upload').click(function(){
		if($('#file').val() == ''){
			return false;
		}

		$(this).removeClass('btn-primary').addClass('btn-danger').html("Processing");

		var bar = $('.bar');
		var percent = $('.percent');
		var status = $('#status');
		$('.progress').show();

		setTimeout(function(){
			$('#f-upload').ajaxSubmit({
				dataType:  'json', 
				url: '<?php echo base_url(); ?>product/admin/gallery_xhr/<?php echo $detail->id; ?>',
				beforeSend: function() {
					status.empty();
					var percentVal = '0%';
					bar.width(percentVal)
					percent.html(percentVal);
				},
				
				uploadProgress: function(event, position, total, percentComplete) {
					var percentVal = percentComplete + '%';
					bar.width(percentVal)
					percent.html(percentVal);
				},
				
				success: processJson
			});
			return false;
		},3000);

	});

	function processJson(data){
		var bar = $('.bar');
		var percent = $('.percent');
		var status = $('#status');
		
		var percentVal = '100%';
		$('#notifier').html(data.message);
		$(this).removeClass('btn-danger').addClass('btn-primary').html("Upload");
		bar.width(percentVal)
		percent.html(percentVal);
		if(data.success == 'success'){
			window.location="<?php echo base_url(); ?>product/admin/gallery/<?php echo $detail->id; ?>"
		} else {
			return false;
		}
	}

	$(document).ready(function(){
	  $('.bxslider').bxSlider();
	});
</script>

<?php $this->load->view('admin_footer'); ?>
