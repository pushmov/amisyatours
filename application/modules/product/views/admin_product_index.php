<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div class="heading">
				Product Index
			</div>

			<?php if($this->session->flashdata('success')) : ?>
			<div style="margin:10px 0">
				<div class="alert alert-success">
				    <button type="button" class="close" data-dismiss="alert">&times;</button>
				    <?php echo $this->session->flashdata('success'); ?>
				</div>
			</div>
			<?php endif; ?>

			<div class="main">
				<button onclick="window.location='<?php echo base_url(); ?>product/admin/add/'" class="btn"><i class="icon-plus"></i>Add Product</button>
				<table class="maintable" style="width:90%;border-top:5px solid #71C39A">
					<thead>
						<tr>
							<th>Posted</th>
							<th>Updated</th>
							<th>Nama</th>
							<th>Harga</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<?php foreach($result as $row): ?>
					<tr>
						<td><?php echo date('D, d/M/Y',strtotime($row->date_created)); ?></td>
						<td><?php echo ($row->date_updated == '') ? '-' : date('D, d/M/Y',strtotime($row->date_updated)); ?></td>
						<td><?php echo $row->name; ?></td>
						<td><?php echo $row->price; ?></td>
						<td class="center">
							<button onclick="window.open('<?php echo base_url(); ?>product/detail/<?php echo $row->links; ?>','_blank')" class="btn"><i class="icon-eye-open"></i>Detail</button>
							<button onclick="window.location='<?php echo base_url(); ?>product/admin/edit/<?php echo $row->id; ?>'" class="btn"><i class="icon-edit"></i>Edit</button>
							<button class="btn btn-danger" onclick="return adelete('<?php echo base_url(); ?>product/admin/delete/<?php echo $row->id; ?>')"><i class="icon-remove icon-white"></i>Delete</button>
						</td>
					</tr>
					<?php endforeach; ?>
				
				</table>
				<?php echo $page; ?>
			</div>

		</div>

</div>
<script>
	function adelete(url){
		
		if(confirm('Are you sure ?')){
			alert(url);
			window.location = url;
		}
	}
</script>
<?php $this->load->view('admin_footer'); ?>
