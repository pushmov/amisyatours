<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MY_Controller {

	const ITEM_PER_PAGE = 5;

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
	}

	public function lokasi(){

		$this->Appmodel->set_table('options');
		$latitude = $this->Appmodel->fetch_row(NULL,array('name' => 'latitude'));
		$longitude = $this->Appmodel->fetch_row(NULL,array('name' => 'longitude'));

		$arr_lat = json_decode($latitude->value);
		$arr_lon = json_decode($longitude->value);

		$data['latitude'] = $arr_lat->latitude;
		$data['longitude'] = $arr_lon->longitude;

		$data = $this->_common_data($data);
		$this->load->view('lokasi_template',$data);
	}

	public function rental(){
		
		$data['title'] = 'Car Rental / Bus';

		$this->Appmodel->set_table('pages');
		$row = $this->Appmodel->fetch_row(NULL,array('name' => 'rental'));
		$data['row'] = $row;
		$data = $this->_common_data($data);
		$this->load->view('landing_page_template',$data);
	}

	public function antar_jemput(){

		$data['title'] = 'Layanan Antar Jemput';

		$this->Appmodel->set_table('pages');
		$row = $this->Appmodel->fetch_row(NULL,array('name' => 'antar_jemput'));
		$data['row'] = $row;
		$data = $this->_common_data($data);
		$this->load->view('landing_page_template',$data);

	}

	public function prosedur_pemesanan(){
		$data['title'] = 'Prosedur Pemesanan via Telpon';

		$this->Appmodel->set_table('pages');
		$row = $this->Appmodel->fetch_row(NULL,array('name' => 'prosedur'));
		$data['row'] = $row;
		$data = $this->_common_data($data);
		$this->load->view('landing_page_template',$data);
	}

	public function domestik($page=0){
		$filter = array('tipe' => 'D');

		$this->Appmodel->set_table('paket_tours');
		$all = $this->Appmodel->all_desc(NULL, $filter);

		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'domestik/';
		$config['total_rows'] = count($all);
		$config['per_page'] = self::ITEM_PER_PAGE;
		$config['cur_tag_open'] = '<span class="yellow" style="margin:0 10px">';
		$config['cur_tag_close'] = '</span>';
		$config['next_page'] = '&laquo;';
		$config['prev_page'] = '&raquo;';

		$result = $this->Appmodel->all_desc(NULL, $filter, $page, self::ITEM_PER_PAGE);

		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();

		$data['result'] = $result;
		$data['tipe'] = 'D';
		$data['all'] = sizeof($all);

		$data = $this->_common_data($data);

		$this->load->view('pages_paket_tours_template',$data);

	}

	public function luar_negri($page=0){
		$filter = array('tipe' => 'L');

		$this->Appmodel->set_table('paket_tours');
		$all = $this->Appmodel->all_desc(NULL, $filter);

		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'luar-negri/';
		$config['total_rows'] = count($all);
		$config['per_page'] = self::ITEM_PER_PAGE;
		$config['cur_tag_open'] = '<span class="yellow" style="margin:0 10px">';
		$config['cur_tag_close'] = '</span>';
		$config['next_page'] = '&laquo;';
		$config['prev_page'] = '&raquo;';

		$result = $this->Appmodel->all_desc(NULL, $filter, $page, self::ITEM_PER_PAGE);

		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();

		$data['result'] = $result;
		$data['tipe'] = 'L';
		$data['all'] = sizeof($all);
		$data = $this->_common_data($data);

		$this->load->view('pages_paket_tours_template',$data);


	}

	public function detail($slug=NULL){
		if(!isset($slug)){
			redirect('/');
		}

		$this->Appmodel->set_table('paket_tours');
		$row = $this->Appmodel->fetch_row(NULL,array('link' => $slug));

		if(empty($row)){
			redirect('/');
		}

		$data['detail'] = $row;
		$data = $this->_common_data($data);

		$this->load->view('detail_tours',$data);

	}

}