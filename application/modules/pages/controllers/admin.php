<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller {

	const ITEM_PER_PAGE = 5;
	const MAX_ALLOWED_SHIPMENT_PICTURE = 5120;
	const FILE_ALLOWED_TYPES = 'jpg|jpeg|bmp|gif|png|JPG|JPEG|BMP|GIF|PNG';

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
	}

	private function _generate_link($string){
		$result = preg_replace("/[^a-zA-Z0-9\s]+/", "", $string);
		$link = strtolower(str_replace(' ', '-', $result));

		return $link;
	}

	public function domestik($page=0){

		$filter = array('tipe' => 'D');
		
		$this->Appmodel->set_table('paket_tours');
		$all = $this->Appmodel->all_desc(NULL, $filter);

		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'pages/admin/domestik/';
		$config['total_rows'] = count($all);
		$config['per_page'] = self::ITEM_PER_PAGE;
		$config['cur_tag_open'] = '<span class="yellow" style="margin:0 10px">';
		$config['cur_tag_close'] = '</span>';
		$config['next_page'] = '&laquo;';
		$config['prev_page'] = '&raquo;';

		$result = $this->Appmodel->all_desc(NULL, $filter, $page, self::ITEM_PER_PAGE);

		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();

		$data['result'] = $result;
		$data['tipe'] = 'D';

		$this->load->view('admin_paket_tours_index',$data);

	}


	public function luar_negri($page=0){
		$filter = array('tipe' => 'L');

		$this->Appmodel->set_table('paket_tours');
		$all = $this->Appmodel->all_desc(NULL, $filter);

		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'pages/admin/domestik/';
		$config['total_rows'] = count($all);
		$config['per_page'] = self::ITEM_PER_PAGE;
		$config['cur_tag_open'] = '<span class="yellow" style="margin:0 10px">';
		$config['cur_tag_close'] = '</span>';
		$config['next_page'] = '&laquo;';
		$config['prev_page'] = '&raquo;';

		$result = $this->Appmodel->all_desc(NULL, $filter, $page, self::ITEM_PER_PAGE);

		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();

		$data['result'] = $result;
		$data['tipe'] = 'L';

		$this->load->view('admin_paket_tours_index',$data);
	}

	public function add_paket_tours(){

		$this->load->view('admin_paket_tours_add');

	}

	public function paket_tours_save(){

		$request = array(
			'title' => $this->input->post('title'),
			'content' => $this->input->post('content'),
			'author' => 1,
			'link' => $this->_generate_link($this->input->post('title')),
			'tipe' => $this->input->post('tipe')
		);
		
		$this->Appmodel->set_table('paket_tours');
		$id = $this->Appmodel->insert($request);

		$name = (!empty($_FILES['gallery']['tmp_name'])) ? time() : '';

		if($name != ''){

			$config['allowed_types'] = self::FILE_ALLOWED_TYPES;
			$config['max_size'] = self::MAX_ALLOWED_SHIPMENT_PICTURE;
			$config['upload_path'] = './public/images/paket_tours/';
			$config['file_name'] = $name;
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('gallery'))
			{
				echo $this->upload->display_errors();
				exit();
			}
			else
			{
				$upload_data = $this->upload->data();
				$photo1 = $upload_data['orig_name'];
			}

			$gallery = $upload_data['orig_name'];
			$update_row = array( 'gallery' => $gallery);
			$where = array('id' => $id);

			$this->Appmodel->update($update_row, $where);

		}		

		$this->session->set_flashdata('success','Data berhasil disimpan');
		$redirectto = ($this->input->post('tipe') == 'D') ? '/pages/admin/domestik/' : '/pages/admin/luar_negri/';
		redirect($redirectto);
	}

	public function edit_paket_tours($id=NULL){

		if(!isset($id)){
			redirect('dashboard/admin');
		}

		$this->Appmodel->set_table('paket_tours');
		$row = $this->Appmodel->fetch_row(NULL,array('id' => $id));

		$data['detail'] = $row;

		$this->load->view('admin_paket_tours_edit',$data);
	}

	public function paket_tours_update(){

		$id = $this->input->post('id');

		$request = array(
			'title' => $this->input->post('title'),
			'content' => $this->input->post('content'),
			'author' => 1,
			'link' => $this->_generate_link($this->input->post('title')),
			'tipe' => $this->input->post('tipe'),
			'date_updated' => date('Y-m-d H:i:s')
		);

		$this->Appmodel->set_table('paket_tours');
		$this->Appmodel->update($request, array('id' => $id));

		$name = (!empty($_FILES['gallery']['tmp_name'])) ? time() : '';

		if($name != ''){

			$config['allowed_types'] = self::FILE_ALLOWED_TYPES;
			$config['max_size'] = self::MAX_ALLOWED_SHIPMENT_PICTURE;
			$config['upload_path'] = './public/images/paket_tours/';
			$config['file_name'] = $name;
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('gallery'))
			{
				echo $this->upload->display_errors();
				exit();
			}
			else
			{
				$upload_data = $this->upload->data();
				$photo1 = $upload_data['orig_name'];
			}

			$gallery = $upload_data['orig_name'];
			$update_row = array( 'gallery' => $gallery);
			$where = array('id' => $id);

			$this->Appmodel->update($update_row, $where);

		}

		$this->session->set_flashdata('success','Data berhasil diupdate');
		$redirectto = ($this->input->post('tipe') == 'D') ? '/pages/admin/domestik/' : '/pages/admin/luar_negri/';
		redirect($redirectto);

	}

	public function delete($id=NULL){
		if(!isset($id)){
			redirect('dashboard/admin');
		}

		$this->Appmodel->set_table('paket_tours');
		$check = $this->Appmodel->fetch_row(NULL,array('id' => $id));
		if(empty($check)){
			redirect('dashboard/admin');
		}

		unlink('./public/images/paket/'.$check->gallery);
		$this->Appmodel->delete(array('id' => $id));

		redirect('dashboard/admin');
	}

	public function rental(){
		$this->Appmodel->set_table('pages');
		$row = $this->Appmodel->fetch_row(NULL,array('name' => 'rental'));
		$data['row'] = $row;
		$data['title'] = 'Car Rental / Bus Page';
		$data['redirect'] = '/pages/admin/rental/';
		$this->load->view('single_page_template',$data);
	}

	public function antar_jemput(){
		$this->Appmodel->set_table('pages');
		$row = $this->Appmodel->fetch_row(NULL,array('name' => 'antar_jemput'));
		$data['row'] = $row;
		$data['title'] = 'Layanan Antar Jemput Page';
		$data['redirect'] = '/pages/admin/antar_jemput/';
		$this->load->view('single_page_template',$data);
	}

	public function prosedur(){
		$this->Appmodel->set_table('pages');
		$row = $this->Appmodel->fetch_row(NULL,array('name' => 'prosedur'));
		$data['row'] = $row;
		$data['title'] = 'Prosedur Pemesanan Page';
		$data['redirect'] = '/pages/admin/prosedur/';
		$this->load->view('single_page_template',$data);
	}

	public function edit($id=NULL){
		
		if(!isset($id)){
			redirect('/dashboard/admin/');
		}

		$this->Appmodel->set_table('pages');
		$detail = $this->Appmodel->fetch_row(NULL,array('id' => $id));

		if(empty($detail)){
			redirect('/dashboard/admin/');
		}

		$data['row'] = $detail;
		$data['title'] = $detail->name;

		$this->load->view('pages_edit_template',$data);

	}

	public function update(){
		if(!$this->input->post('id')){
			redirect('/pages/admin/rental/');
		}

		$this->Appmodel->set_table('pages');

		$row = $this->Appmodel->fetch_row(NULL,array('id' => $this->input->post('id')));

		$request = array(
			'content' => $this->input->post('content'),
			'author' => 1,
			'date_updated' => date('Y-m-d H:i:s')
		);

		$this->Appmodel->update($request, array('id' => $this->input->post('id')));

		$this->session->set_flashdata('success','Konten berhasil di update');
		redirect('/pages/admin/'.$row->name);
	}

	public function replace_picture($id=NULL){
		
		if(!isset($id)){
			redirect('/dashboard/admin/');
		}

		$this->Appmodel->set_table('paket_tours');
		$row = $this->Appmodel->fetch_row(NULL,array('id' => $id));

		if(empty($row)){
			redirect('/dashboard/admin/');
		}

		unlink('./public/images/paket_tours/' . $row->gallery);

		$gallery_update = array('gallery' => NULL);

		$this->Appmodel->update($gallery_update, array('id' => $id));
		$this->session->set_flashdata('Index gallery berhasil di hapus');

		redirect('pages/admin/edit_paket_tours/' . $id);
	}

}