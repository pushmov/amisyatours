<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Umrah extends MY_Controller {

	const BIAYA_EKONOMIS = 1750.00;
	const BIAYA_REGULER = 1950.00;
	const BIAYA_EKSEKUTIF = 2350.00;
	const BIAYA_TURKEY = 2850.00;

	var $name = 'umrah';
	var $modules = 'Umrah';

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
		$this->Appmodel->set_table('umrah');
	}

	public function about(){
		$name = 'about';
		$title = 'Mengenai Umrah';
		$row = $this->Appmodel->fetch_row(NULL,array('name' => $name));

		$data['row'] = $row;
		$data['modules'] = $this->modules;
		$data['title'] = $title;

		$data = $this->_common_data($data);
		$this->load->view('pages_template',$data);
	}

	public function persyaratan(){
		$name = 'persyaratan';
		$title = 'Persyaratan Umrah';
		$row = $this->Appmodel->fetch_row(NULL,array('name' => $name));

		$data['row'] = $row;
		$data['modules'] = $this->modules;
		$data['title'] = $title;

		$data = $this->_common_data($data);
		$this->load->view('pages_template',$data);
	}

	public function biaya(){
		$name = 'biaya';
		$title = 'Biaya Umrah';
		$row = $this->Appmodel->fetch_row(NULL,array('name' => $name));

		$data['row'] = $row;
		$data['modules'] = $this->modules;
		$data['title'] = $title;
		$data = $this->_common_data($data);
		$this->load->view('pages_template',$data);
	}

	public function calculate(){
		$data['modules'] = $this->modules;
		$data['title'] = 'a';
		$request = array(
			'currency' => $this->input->post('currency'),
			'paket' => $this->paket($this->input->post('paket')),
			'kamar' => $this->input->post('kamar'),
			'jamaah' => $this->input->post('jamaah'),
			'harga' => $this->harga($this->input->post('kamar'), $this->input->post('jamaah'),$this->input->post('paket'), $this->input->post('currency'))
		);
		

		$data['request'] = $request;
		$data = $this->_common_data($data);
		$this->load->view('form_template',$data);

	}

	private function harga($kamar, $jamaah,$paket,$currency){
			$dollar = 0.00;

			switch($paket){
				case 'eko':
					$dollar = self::BIAYA_EKONOMIS;
					if($kamar != 'Q'){
						$dollar += ($kamar == 'D') ? 200.00 : 100.00;
					}
					break;
				case 'reg':
					$dollar = self::BIAYA_REGULER;
					if($kamar != 'Q'){
						$dollar += ($kamar == 'D') ? 250.00 : 125.00;
					}
					
					break;
				case 'eks':
					$dollar = self::BIAYA_EKSEKUTIF;
					if($kamar != 'Q'){
						$dollar += ($kamar == 'D') ? 350.00 : 175.00;
					}
					break;
				case 'tur':
					$dollar = self::BIAYA_TURKEY;
					if($kamar != 'Q'){
						$dollar += ($kamar == 'D') ? 300.00 : 150.00;
					}
					break;
			}

			$dollar = $dollar * $jamaah;

			if($currency == 'IDR'){
				
				$dollar = 'Rp. '. $this->convert_currency($dollar);
			} elseif($currency == 'USD'){
				$dollar = 'USD ' . number_format($dollar,2);
			}

			return $dollar;
	}

	public function ajax_calculate(){

		if($this->is_ajax() || 1){

			$response = array();
			
			$currency = $this->input->post('currency');
			$kamar = $this->input->post('kamar');
			$paket = $this->input->post('paket');
			$jamaah = $this->input->post('jamaah');

			$dollar = $this->harga($kamar, $jamaah, $paket,$currency);

			

			$response['result'] = $dollar;
			echo json_encode($response);

		}
		
	}

	


}