	<?php $this->load->view('header'); ?>
	<?php $this->load->view('plugins/flexnav'); ?>
	<?php $this->load->view('plugins/datepicker'); ?>
	<?php $this->load->view('header_end'); ?>
	<?php $this->load->view('statics/menu'); ?>
				

				<h5 style="color:#333333; font-weight:400 !important"><a style="color:#333333 !important; font-weight:400 !important" href="<?php echo base_url(); ?>">
					<i class="icon-home" style="margin-right:5px;"></i>Home</a> > <?php echo $modules; ?> > <?php echo $title; ?>
				</h5>
				<div class="content" style="padding: 10px 20px !important">
					
					<h2>FORM PENDAFTARAN UMROH</h2>
					<div id="desc">
						<p>Paket Umroh yang anda pilih adalah <?php echo $request['paket']; ?></p>
						<p>Calon Jamaah Umroh: <?php echo $request['jamaah']; ?> orang</p>
						<p>Total biaya Umroh: <?php echo $request['harga']; ?> </p>

						
					</div>

					<div class="clearfix">
						<form name="form-template" id="form-template" action="<?php echo base_url(); ?>welcome/contact/submit_jamaah/" method="POST">
						<table style="width:100%" align="left">
							<?php $counter = 1; ?>
							<?php for($i=1;$i<=$request['jamaah'];$i++) : ?>
							<tr style="margin:5px 0;">
								<td style="width:5%"><?php echo $counter; ?></td>
								<td class="left" style="width:30%">
									<div style="height:35px">Gender : </div>
									<div style="height:35px">Nama (sesuai passport) : </div>
									<div style="height:35px">Nomor Passport : </div>
									<div style="height:35px">Tanggal Lahir (sesuai passport) : </div>
								</td>
								<td class="left">
									<div>
										<input type="radio" class="jk" name="jk[<?php echo $counter; ?>]" id="jk" value="P" checked>
										<span>Pria</span>
										<input type="radio" class="jk" name="jk[<?php echo $counter; ?>]" id="jk" value="W">
										<span>Wanita</span>
									</div>
									<div><input type="text" name="nama[<?php echo $counter; ?>]" id="nama[<?php echo $counter; ?>]" value="" style="width:50%"></div>
									<div><input type="text" name="passport[<?php echo $counter; ?>]" id="passport[<?php echo $counter; ?>]" value="" style="width:50%"></div>
									<div><input type="text" class="datepicker" name="dob[<?php echo $counter; ?>]" id="dob[<?php echo $counter; ?>]" value="" style="width:50%"></div>
								</td>
							</tr>
							<?php $counter++; ?>
							<?php endfor; ?>
							<tr>
								<td class="left" colspan="2">
									Email<span class="red">*</span> : 
								</td>
								<td class="left">
									<input type="text" name="email" id="email" value="">
								</td>
							</tr>
							<tr>
								<td class="center" colspan="3">
									<button id="step3-btn" class="btn btn-primary"><i class="icon-ok icon-white"></i>SUBMIT</button>
								</td>
							</tr>
						</table>
					</form>
					</div>

					
				</div>
					
				</div>
				
				
				</section>
			</div>
			
			<?php $this->load->view('statics/footer'); ?>
		

	<?php $this->load->view('html_end'); ?>