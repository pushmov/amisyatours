	<?php $this->load->view('header'); ?>
	<?php $this->load->view('plugins/flexnav'); ?>
	<?php $this->load->view('header_end'); ?>
	<?php $this->load->view('statics/menu'); ?>
				

				<h5 style="color:#333333; font-weight:400 !important"><a style="color:#333333 !important; font-weight:400 !important" href="<?php echo base_url(); ?>">
					<i class="icon-home" style="margin-right:5px;"></i>Home</a> > <?php echo $modules; ?> > <?php echo $title; ?>
				</h5>
				<div class="content" style="padding: 10px 20px !important">
					
					<?php echo $row->content; ?>

					
				</div>
				</div>
				
					<div class="calculator center" style="background:#DEDEDE;padding:30px 0">
						
						<form id="calculate" name="calculate" action="<?php echo base_url(); ?>pages/<?php echo strtolower($modules); ?>/calculate/" method="POST">
							<div id="step1">
								<h2>FORM PENDAFTARAN <?php echo strtoupper($modules); ?></h2>
								<table style="width:75%" align="center">
									<tr>
										<td class="left" style="width:35%">Mata Uang</td>
										<td class="left" style="padding:5px 0">
											<input type="radio" class="currency" name="currency" id="currency" value="USD" checked><img src="<?php echo base_url(); ?>public/images/us.png" width="32">
											<span>US Dollar</span>
											<input type="radio" class="currency" name="currency" id="currency" value="IDR"><img src="<?php echo base_url(); ?>public/images/id.png" width="32">
											<span>Rupiah Indonesia</span>
										</td>
									</tr>
									<tr>
										<td class="left">Paket</td>
										<td class="left">
											<select name="paket" id="paket">
												<option value="eko">Ekonomis</option>
												<option value="reg">Reguler</option>
												<option value="eks">Eksekutif</option>
												<option value="tur">Umrah + Turkey</option>
											</select>
										</td>
									</tr>
									<tr>
										<td class="left">Kamar</td>
										<td class="left">
											<input type="radio" name="kamar" id="kamar" value="Q" checked>
											<span>Quad (1 Kamar 4 Orang)</span>
											
											<input type="radio" name="kamar" id="kamar" value="T">
											<span>Triple (1 Kamar 3 Orang)</span>
											<input type="radio" name="kamar" id="kamar" value="D">
											<span>Double (1 Kamar 2 Orang)</span>
										</td>
									</tr>
									<tr>
										<td class="left">Calon Jamaah</td>
										<td class="left"><input type="text" name="jamaah" id="jamaah" value="1" placeholder="ex. 1" style="width:6%"> Orang</td>
									</tr>
									<tr>
										<td class="left">Total Biaya</td>
										<td class="left" style="color:#11B997" id="result"><h1></h1></td>
									</tr>
									<tr>
										<td colspan="2"><button id="continue" class="btn btn-primary"><i class="icon-chevron-right icon-white"></i>PROSES</button></td>
									</tr>
								</table>
							</div>
							<div id="step2" style="display:none">
								
								<h2>FORM PENDAFTARAN UMROH</h2>
								<div id="desc">
									<p>Paket Umroh yang anda pilih adalah ekonomi (4-6 pax/kmr)</p>
									<p>Calon Jamaah Umroh: 3 orang (3 dewasa)</p>

									<p>Total biaya Umroh: $ 6.300 </p>
								</div>

								<button id="back" class="btn btn-danger"><i class="icon-chevron-left icon-white"></i>BACK</button>
								<button id="step3-btn" class="btn btn-primary">CONTINUE<i class="icon-chevron-right icon-white"></i></button>
							</div>
						</form>
					</div>
					
					
				</section>
			</div>
			
			<?php $this->load->view('statics/footer'); ?>
		

		<script>

		$(window).load(function(){
			calc_amount();
		});

		$('#continue').click(function(){
			$('#step1').hide();
			$('#step2').show();

			

			return false;
		});

		$('#back').click(function(){
			$('#step1').show();
			$('#step2').hide();
			return false;
		});

		$(document).ready(function(){
			calc_amount();
		});
		
		$('#calculate').change(function(){
			calc_amount();
		});
		
		$('input[name=currency],input[name=kamar]').on('ifChecked', function(event){
			calc_amount();
		});

		function calc_amount(){
			$('#result').html("<h1>loading...</h1>");
			var curr = $('input[name=currency]:checked', '#calculate').val();
			var kamar = $('input[name=kamar]:checked', '#calculate').val();
			var paket = $('#paket').val();
			
			var jamaah = $('#jamaah').val();

			if(parseInt(jamaah) <= 0 ){
				return false;
			}

			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>pages/umrah/ajax_calculate/",
				data: { currency: curr, kamar: kamar, paket:paket, jamaah:jamaah },
				context: document.body
			}).done(function(data) {
				
				var result = $.parseJSON(data);
				$( '#result' ).html( "<h1>"+result.result+"</h1>" );
				$('#desc').html('<p>Paket Umroh yang anda pilih adalah '+$('#paket option:selected').text()+'</p><p>Calon Jamaah Umroh : '+jamaah+' orang</p><p>Total biaya Umroh : '+result.result+'</p> ');
			});

		};
		</script>
	<?php $this->load->view('html_end'); ?>