	<?php $this->load->view('header'); ?>
	<?php $this->load->view('plugins/flexnav'); ?>
	<?php $this->load->view('plugins/sharethis'); ?>
	<?php $this->load->view('plugins/bxslider'); ?>
	<?php $this->load->view('plugins/prettyphoto'); ?>
	<?php $this->load->view('header_end'); ?>
	<?php $this->load->view('statics/menu'); ?>

				<div class="content" style="padding: 10px 0 !important">
					<div style="padding:25px">
						<div>Home &gt; Paket Tours &gt; <?php echo ($tipe == 'D') ? 'Domestik' : 'Luar Negri'; ?></div>
						<?php if(empty($result)) : ?>
						No Records Found. 
						<?php else : ?>
						<div class="clearfix">
							<div style="float:left">
								<h3>Daftar Paket Tours <?php echo ($tipe == 'D') ? 'Domestik' : 'Luar Negri ' ; ?></h3>
								<span style="color:#14B997"><?php echo $all; ?> Records Found. </span>
							</div>
							<div style="float:right">
								<?php echo $page; ?>
							</div>
						</div>
						<?php foreach($result as $row) : ?>
						
						<div class="clearfix" style="margin:20px 0">

							<div>
								
								<div style="font-family:PTSans-regular !important;" class="clearfix">
									<div class="news-list-wrapper">
										<div class="float-left" style="margin-right:15px;margin-bottom:10px;margin-top:10px">
											<img src="<?php echo base_url(); ?>public/images/paket_tours/<?php echo $row->gallery; ?>" width="240">
										</div>
										<div>
											<div class="left fs18" style="margin:10px 0">
												<h5 style="margin:0">On <?php echo date('D, d/M/Y',strtotime($row->date_created)); ?></h5>
												<a href="<?php echo base_url(); ?>pages/detail/<?php echo $row->link; ?>"><span style="color:#2C9192"><?php echo $row->title; ?></span></a>
											</div>
											<?php 
											$string = strip_tags($row->content);

											if (strlen($string) > 150) {

											    // truncate string
											    $stringCut = substr($string, 0, 500);

											    // make sure it ends in a word so assassinate doesn't become ass...
											    $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
											}
											echo $string; 
											?>
										</div>
										<div style="text-align:right"><button onclick="window.location='<?php echo base_url(); ?>pages/detail/<?php echo $row->link; ?>'" class="btn"><i class="icon-eye-open"></i>Read More</button></div>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach; ?>

						<?php endif; ?>

						<?php echo $page; ?>

						<!--gallery box-->
						
					</div>
				</div>
				</section>
			</div>
				<?php $this->load->view('statics/footer'); ?>
		</div>

	<?php $this->load->view('html_end'); ?>