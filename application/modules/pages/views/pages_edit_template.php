<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div class="heading" style="margin:10px 0">
				<?php echo $title; ?> - Edit
			</div>


			<div class="clearfix" style="margin:20px 0">
				<?php if($row->date_updated != '') :?>
				<div class="float-left"><h6>Last Update : <?php echo date('D, d/M/Y',strtotime($row->date_updated)); ?></h6></div>
				<?php endif; ?>
			</div>

			<form name="editpage" id="editpage" action="<?php echo base_url();?>pages/admin/update/" method="POST" >
				<input type="hidden" name="id" value="<?php echo $row->id; ?>">
				
				<textarea style="height:400px" name="content" id="content">
					<?php echo $row->content; ?>
				</textarea>
				<div style="margin:10px 0" class="right"><button id="submit" class="btn btn-primary"><i class="icon-edit icon-white"></i>Simpan</button></div>
			</form>
		</div>
</div>
<script>
	$('#submit').click(function(){
		if($('#slug').val() == '')
			return false;
		$('#editpage').submit();
	});
</script>
<?php $this->load->view('admin_footer'); ?>
