<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<h2>Update Paket Tours : <?php echo $detail->title; ?></h2>

			<div id="ajax-response">
				<form enctype="multipart/form-data" name="update-paket-tours" id="update-paket-tours" method="POST" action="<?php echo base_url(); ?>pages/admin/paket_tours_update/">
				<input type="hidden" name="id" value="<?php echo $detail->id; ?>" id="id">
				<table class="maintable" style="width:100%;border-top:4px solid #71C39A">
					
					<tr>
						<?php if($detail->gallery != '') : ?>
						<td>Display Picture</td>
						<td colspan="2">
							<img src="<?php echo base_url(); ?>public/images/paket_tours/<?php echo $detail->gallery; ?>" width="160">
							<div><a href="<?php echo base_url(); ?>pages/admin/replace_picture/<?php echo $detail->id; ?>" class="btn"><i class="icon-refresh"></i>Change Picture</a></div>
						</td>
						<?php else : ?>
						<td>Display Picture<span class="red">*</span></td>
						<td><input type="file" name="gallery" id="gallery" ></td>
						<?php endif; ?>
						
					</tr>

					<tr>
						<td>Title<span class="red">*</span></td>
						<td><input style="width:80%" type="text" name="title" id="title" value="<?php echo $detail->title; ?>"></td>
					</tr>
					<tr>
						<td>Description<span class="red">*</span></td>
						<td><textarea name="content" id="content" style="width:100%;height:358px;"><?php echo $detail->content; ?></textarea></td>
					</tr>

					<tr>
						<td>Tipe<span class="red">*</span></td>
						<td><input <?php echo ($detail->tipe =='D') ? 'checked' : '';  ?> type="radio" name="tipe" value="D" id="D"><span>Domestik</span><input <?php echo ($detail->tipe =='L') ? 'checked' : '';  ?> type="radio" name="tipe" value="L" id="L" ><span>Luar Negri</span></td>
					</tr>
					
					<tr>
						<td colspan="2" class="right"><button class="btn btn-primary"><i class="icon-ok icon-white"></i>Update</button></td>
					</tr>
				</table>
				</form>
				<div>Field dengan mark (<span class="red">*</span>) wajib diisi</div>
			</div>
		</div>

</div>
<script>
	$(document).ready(function(){
		$('#content').val('<?php echo $detail->content; ?>');
	});
</script>
<?php $this->load->view('admin_footer'); ?>
