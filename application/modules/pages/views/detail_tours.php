	<?php $this->load->view('header'); ?>
	<?php $this->load->view('plugins/flexnav'); ?>
	<?php $this->load->view('plugins/sharethis'); ?>
	<?php $this->load->view('plugins/bxslider'); ?>
	<?php $this->load->view('plugins/prettyphoto'); ?>
	<?php $this->load->view('header_end'); ?>
	<?php $this->load->view('statics/menu'); ?>

				<div class="content" style="padding: 10px 0 !important">
					<div style="padding:25px">
						<div>Home &gt; Paket Tours &gt; <?php echo ($detail->tipe == 'D') ? 'Domestik' : 'Luar Negri'; ?></div>
						<div class="left fs24" style="margin:10px 0"><span style="color:#2C9192"><?php echo $detail->title; ?></span></div>
						<?php if($detail->date_updated != '') :?>
						<p style="font-weight:400;font-family:PTSans-regular !important">Last Update : <?php echo date('D, d/M/Y H:i:s',strtotime($detail->date_updated)); ?></p>
						<?php endif; ?>

						<div class="clearfix" style="margin:20px 0">
							<div class="float-left" style="margin-right:20px;width:35%">
								

							</div>
							<div>

								
								<div style="font-family:PTSans-regular !important;" class="clearfix">
									<div style="text-align:center;padding:20px 0">
										<img src="<?php echo base_url(); ?>public/images/paket_tours/<?php echo $detail->gallery; ?>">
									</div>
									<?php echo $detail->content; ?>
								</div>
							</div>
						</div>

						<!--gallery box-->
						

						
						<div class="center">
							<p style="font-weight:900 !important;font-family:PTSans-regular !important;font-size:18px">Share This :</p>
							<div class="" style="margin:10px 0">
								<span class='st_sharethis_large' displayText='ShareThis'></span>
								<span class='st_facebook_large' displayText='Facebook'></span>
								<span class='st_twitter_large' displayText='Tweet'></span>
								<span class='st_googleplus_large' displayText='Google +'></span>
								<span class='st_linkedin_large' displayText='LinkedIn'></span>
								<span class='st_pinterest_large' displayText='Pinterest'></span>
							</div>
						</div>
					</div>
				</div>
				</section>
			</div>
				<?php $this->load->view('statics/footer'); ?>
		</div>

		<script>
		$(document).ready(function(){
			$('table').attr('width','100%');
		});
		</script>

	<?php $this->load->view('html_end'); ?>