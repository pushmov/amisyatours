<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<h2>Tambah Paket Tours</h2>

			<div id="ajax-response">
				<form enctype="multipart/form-data" name="paket-tours" id="paket-tours" method="POST" action="<?php echo base_url(); ?>pages/admin/paket_tours_save/">
				<table class="maintable" style="width:100%;border-top:4px solid #71C39A">
					
					<tr>
						<td>Display Picture (160x160 px)<span class="red">*</span></td>
						<td><input type="file" name="gallery" id="gallery" ></td>
					</tr>

					<tr>
						<td>Title<span class="red">*</span></td>
						<td><input style="width:80%" type="text" name="title" id="title" ></td>
					</tr>
					<tr>
						<td>Description<span class="red">*</span></td>
						<td><textarea name="content" id="content" style="width:100%;height:358px;"></textarea></td>
					</tr>
					<tr>
						<td>Tipe<span class="red">*</span></td>
						<td><input type="radio" name="tipe" value="D" checked><span>Domestik</span><input type="radio" name="tipe" value="L" ><span>Luar Negri</span></td>
					</tr>
					
					<tr>
						<td colspan="2" class="right"><button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i>Create</button></td>
					</tr>
				</table>
				</form>
				<div>Field dengan mark (<span class="red">*</span>) wajib diisi</div>
			</div>
		</div>

</div>

<?php $this->load->view('admin_footer'); ?>
