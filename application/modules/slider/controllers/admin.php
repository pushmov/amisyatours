<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller {

	const MAX_ALLOWED_SHIPMENT_PICTURE = 5120;
	const FILE_ALLOWED_TYPES = 'jpg|jpeg|bmp|gif|png|JPG|JPEG|BMP|GIF|PNG';

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
		if(!$this->session->userdata('admin_amisyatours')){
			$this->session->set_flashdata('error','<strong>Error</strong> Your session has expired.');
			redirect('welcome/admin');
		}

	}

	public function index(){
		$this->Appmodel->set_table('slides');
		$result = $this->Appmodel->fetch_rows();

		$data['sliders'] = $result;

		$this->load->view('slider_index',$data);
	}

	public function add(){
		$this->load->view('slider_add');
	}

	public function edit($id=NULL){
		$this->Appmodel->set_table('slides');
		$data['result'] = $this->Appmodel->fetch_row(NULL,array('id' => $id));
		$this->load->view('slider_edit',$data);
	}

	public function delete($id=NULL){
		
		if(!isset($id)){
			redirect('slider/admin/');
		}

		$this->Appmodel->set_table('slides');
		$row = $this->Appmodel->fetch_row(NULL,array('id' => $id));
		if(empty($row)){
			redirect('slider/admin/');
		}

		$this->Appmodel->delete(array('id' => $id));
		/** delete actual files */
		unlink('./public/images/slides/'.$row->slides_name);
		unlink('./public/images/slides/thumbs/'.$row->slides_name);
		redirect('slider/admin/');

	}

	public function xhr_file_upload_handler($id=NULL){
		
		$response = array();
		
		$photo = (!empty($_FILES['file']['tmp_name'])) ? time() . '-1' : '';

		/* upload files process */		
		$config['allowed_types'] = self::FILE_ALLOWED_TYPES;
		$config['max_size'] = self::MAX_ALLOWED_SHIPMENT_PICTURE;
		$config['upload_path'] = './public/images/slides/';
		
		$ci = get_instance();
		if($photo != '')
		{
			$config['file_name'] = $photo;
			$ci->load->library('upload',$config);
			if(!$ci->upload->do_upload('file'))
			{
				$this->session->set_flashdata('errorcode','imageerror');
				$response['success'] = 'fail';
				$response['newimage'] = '';
				$response['errorcode'] = 1;
				$response['message'] = 'Allowed picture : png, jpg or bmp only';
				echo json_encode($response);
				exit();
			}
			else
			{
				$data_photo = $ci->upload->data();

				$photo = $data_photo['file_name'];
				$response['success'] = 'success';

				/** create thumb */
				$config['new_image'] = './public/images/slides/thumbs/'.$photo;
				$config['image_library'] = 'gd2';
				$config['source_image'] = './public/images/slides/'.$photo;
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width'] = 100;
				$config['height'] = 75;
				$this->load->library('image_lib', $config);


				$sess_data = $this->session->userdata('admin_amisyatours');
				
				

				$this->Appmodel->set_table('slides');

				if(isset($id)){
					
					$inser_data = array(
						'created_by'  => $sess_data['id'],
						'slides_name' => $photo,
						'slides_caption' => $this->input->post('caption'),
						'date_updated' => date('Y-m-d H:i:s')
						);
					$this->Appmodel->update($inser_data,array('id' => $id));

				} else {

					$inser_data = array(
						'created_by'  => $sess_data['id'],
						'slides_name' => $photo,
						'slides_caption' => $this->input->post('caption')
						);
					$this->Appmodel->insert($inser_data);

				}

				/** chcking image size */
				$width = $data_photo['image_width'];
				$height = $data_photo['image_height'];
				if($width > 1280 || $height > 800){

					$config['new_image'] = './public/images/slides/'.$photo;
					$config['image_library'] = 'gd2';
					$config['source_image'] = './public/images/slides/'.$photo;
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 1280;
					$config['height'] = 800;

					$this->load->library('image_lib', $config); 
					$this->image_lib->resize();
				}

				echo json_encode($response);
				exit();
			}
		}
		
	
	}


}