<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<div class="clearfix">
				<div class="float-left">Total Records : <?php echo sizeof($sliders); ?></div>
				<div class="float-right">
					<button class="btn" style="color:#000000 !important;font-weight:400" onclick="window.location='<?php echo base_url(); ?>slider/admin/add/'"><span class="icon-plus"></span> Add new item</button>
				</div>
			</div>
			<div>
				<table class="maintable" style="border-top:5px solid #71C39A">
						<tr>
							<?php $counter = 1; ?>
							<?php foreach($sliders as $row) : ?>
							<td>
								<div class="slider-wrapper clearfix">
									<img src="<?php echo base_url()?>public/plugin/imagecache.php?width=400&amp;height=75&amp;cropratio=1:1&image=<?php echo base_url(); ?>public/images/slides/<?php echo $row->slides_name; ?>">
									<div class="float-right">
										<div style="margin:10px 0"><a style="color:#000000 !important" id="submit" href="<?php echo base_url(); ?>slider/admin/edit/<?php echo $row->id; ?>"><span class="icon-edit"></span> Edit</a></div>
										<div style="margin:10px 0"><a onclick="return confirm('Are you sure want to delete the record?')" style="color:#000000 !important" id="submit" href="<?php echo base_url(); ?>slider/admin/delete/<?php echo $row->id; ?>"><span class="icon-remove"></span> Delete</a></div>
									</div>
								</div>
								<div style="margin:10px 0;padding:5px 10px;background:#444444; opacity:0.7;color:#FFFFFF"><?php echo $row->slides_caption; ?></div>
							</td>
							<?php if($counter % 4 == 0) : ?>
							</tr><tr>
							<?php endif; ?>
							<?php $counter++; ?>
							<?php endforeach; ?>
						</tr>
				</table>
			</div>
		</div>

</div>
<?php $this->load->view('admin_footer'); ?>
