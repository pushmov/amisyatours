<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<h3>Edit slide</h3>
			<div>
				<form action="<?php echo base_url(); ?>slider/admin/add/" method="POST" name="slider-add" id="slider-add">
				<table class="maintable" style="width:70%;margin:0;border-top:5px solid #71C39A">
					<tr>
						<td>Change picture :</td>
						<td>
							<input type="file" name="file" id="file" >
							<div id="notifier" style="display:none">Uploading ...</div>
							<div id="progress-1" class="progress">
								<div class="bar"><span class="percent"></span></div >
							</div>
						</td>
					</tr>
					<tr>
						<td>Text Caption :</td>
						<td><input type="text" name="caption" id="caption" value="<?php echo $result->slides_caption; ?>"></td>
					</tr>
					<tr>
						<td colspan="2" class="right">
							<A id="submit" class="btn btn-sm btn-primary" style="color:#FFFFFF !important"><i class="icon-ok-circle icon-white"></i> Simpan</A>
						</td>
					</tr>
				</table>
			</div>
		</div>

</div>
<script>
	$('#submit').click(function(){
		
		if($('#file').val() == ''){
			return false;
		}
		if( $('#caption').val() == ''){
			return false;
		}
		$('#notifier').show();
		$(this).removeClass('btn-primary').addClass('btn-danger').html("Processing");

		var bar = $('.bar');
		var percent = $('.percent');
		var status = $('#status');
		$('.progress').show();

		setTimeout(function(){
			$('#slider-add').ajaxSubmit({
				dataType:  'json', 
				url: '<?php echo base_url(); ?>slider/admin/xhr_file_upload_handler/<?php echo $result->id; ?>',
				beforeSend: function() {
					status.empty();
					var percentVal = '0%';
					bar.width(percentVal)
					percent.html(percentVal);
				},
				
				uploadProgress: function(event, position, total, percentComplete) {
					var percentVal = percentComplete + '%';
					bar.width(percentVal)
					percent.html(percentVal);
				},
				
				success: processJson
			});
			return false;
		},3000);
	});

	function processJson(data){
		var bar = $('.bar');
		var percent = $('.percent');
		var status = $('#status');
		
		var percentVal = '100%';
		$('#notifier').html(data.success);
		
		bar.width(percentVal)
		percent.html(percentVal);
		$(this).removeClass('btn-danger').addClass('btn-primary').html("Simpan");
		if(data.success == 'success'){
			window.location="<?php echo base_url(); ?>slider/admin/"
		}
	}

</script>
<?php $this->load->view('admin_footer'); ?>
