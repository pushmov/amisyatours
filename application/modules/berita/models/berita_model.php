<?php

Class Berita_model extends Appmodel
{

	function get_berita($offset=NULL,$limit=NULL){
		$this->db->order_by('date_created DESC');

		return $this->db->get($this->table,$limit, $offset)
		->result();

	}

}
