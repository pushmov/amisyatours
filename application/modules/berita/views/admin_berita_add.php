<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<h2>Tambah Berita</h2>

			<div id="ajax-response">
				<form enctype="multipart/form-data" name="berita-product" id="berita-product" method="POST" action="<?php echo base_url(); ?>berita/admin/save/">
				<table class="maintable" style="width:100%;border-top:4px solid #71C39A">
					
					<tr>
						<td>Display Picture (160x160 px)<span class="red">*</span></td>
						<td><input type="file" name="gallery" id="gallery" ></td>
					</tr>

					<tr>
						<td>Judul<span class="red">*</span></td>
						<td><input style="width:80%" type="text" name="judul" id="judul" ></td>
					</tr>
					<tr>
						<td>Description<span class="red">*</span></td>
						<td><textarea name="content" id="content" style="width:100%;height:358px;"></textarea></td>
					</tr>
					
					<tr>
						<td colspan="2" class="right"><button class="btn btn-primary"><i class="icon-ok icon-white"></i>Create</button></td>
					</tr>
				</table>
				</form>
				<div>Field dengan mark (<span class="red">*</span>) wajib diisi</div>
			</div>
		</div>

</div>

<?php $this->load->view('admin_footer'); ?>
