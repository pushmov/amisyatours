<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller {

	const MAX_ALLOWED_SHIPMENT_PICTURE = 5120;
	const FILE_ALLOWED_TYPES = 'jpg|jpeg|bmp|gif|png|JPG|JPEG|BMP|GIF|PNG';
	const ITEM_PER_PAGE = 10;

	public function __construct(){
		parent::__construct();
		$this->load->model('Berita_model');
		if(!$this->session->userdata('admin_amisyatours')){
			$this->session->set_flashdata('error','<strong>Error</strong> Your session has expired.');
			redirect('welcome/admin');
		}

		$this->Berita_model->set_table('news');
	}

	public function index($page=0){

		//admin list berita by table
		$count_news = $this->Berita_model->fetch_rows();
		
		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'berita/admin/index/';
		$config['total_rows'] = count($count_news);
		$config['per_page'] = self::ITEM_PER_PAGE;
		$config['cur_tag_open'] = '<span class="yellow" style="margin:0 10px">';
		$config['cur_tag_close'] = '</span>';
		$config['next_page'] = '&laquo;';
		$config['prev_page'] = '&raquo;';

		$result = $this->Berita_model->get_berita($page,self::ITEM_PER_PAGE);

		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();

		$data['result'] = $result;
		$this->load->view('admin_berita_index',$data);
	}

	public function add(){

		$this->load->view('admin_berita_add');

	}

	public function edit($id=NULL){
		
		if(!isset($id)){
			redirect('berita/admin/index');
		}

		$this->Berita_model->set_table('news');
		$detail = $this->Berita_model->fetch_row(NULL,array('id' => $id));
		if(empty($detail)){
			redirect('berita/admin/index');
		}

		$data['detail'] = $detail;
		$this->load->view('admin_berita_edit',$data);

	}

	public function replace_picture($id=NULL){
		if(!isset($id)){
			redirect('berita/admin/index');
		}

		$row = $this->Berita_model->fetch_row(NULL,array('id' => $id));
		unlink('./public/images/berita/'.$row->gallery);
		$this->Berita_model->update(array('gallery' => ''), array('id' => $id));
		redirect('berita/admin/edit/'.$id);
	}

	public function save(){

		$name = (!empty($_FILES['gallery']['tmp_name'])) ? time() : '';

		if($name != ''){

			$config['allowed_types'] = self::FILE_ALLOWED_TYPES;
			$config['max_size'] = self::MAX_ALLOWED_SHIPMENT_PICTURE;
			$config['upload_path'] = './public/images/berita/';
			$config['file_name'] = $name;
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('gallery'))
			{
				echo $this->upload->display_errors();
				exit();
			}
			else
			{
				$upload_data = $this->upload->data();
				$photo1 = $upload_data['orig_name'];
			}

		}

		$request = array(
			'title' => $this->input->post('judul'),
			'content' => $this->input->post('content'),
			'author' => 1,
			'gallery' => $photo1
		);
		$id = $this->Berita_model->insert($request);

		$links = date('Y/m/d') . '/'.strtolower(str_replace(' ', '-', $this->input->post('judul'))).'/'.$id.'/';
		$this->Berita_model->update(array('link' => $links), array('id' => $id));
		
		$this->session->set_flashdata('success','Data berhasil di simpan');
		redirect('berita/admin/index');
	}

	public function update(){

		if(!$this->input->post('id')){
			redirect('berita/admin/index/');
		}
		$id = $this->input->post('id');

		$row = $this->Berita_model->fetch_row(NULL,array('id' => $id));

		$name = (!empty($_FILES['gallery']['tmp_name'])) ? time() : '';
		if($name == ''){
			
			$photo = $row->gallery;

		} else {
			
			
			$config['allowed_types'] = self::FILE_ALLOWED_TYPES;
			$config['max_size'] = self::MAX_ALLOWED_SHIPMENT_PICTURE;
			$config['upload_path'] = './public/images/berita/';
			$config['file_name'] = $name;
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('gallery'))
			{
				echo $this->upload->display_errors();
				exit();
			}
			else
			{
				$upload_data = $this->upload->data();
				$photo = $upload_data['orig_name'];
			}

		}
		
		$request = array(
			'title' => $this->input->post('judul'),
			'content' => $this->input->post('content'),
			'date_updated' => date('Y-m-d H:i:s'),
			'gallery' => $photo
		);
		$this->Berita_model->update($request, array('id' => $id));

		$this->session->set_flashdata('success','Data berhasil di update');
		redirect('berita/admin/index');
	}

	public function search(){}

	public function delete($id=NULL){
		
		if(!isset($id)){
			redirect('berita/admin/index');
		}

		$this->Berita_model->set_table('news');
		$row = $this->Berita_model->fetch_row(NULL,array('id' => $id));
		if(empty($row)){
			redirect('berita/admin/index');
		}

		unlink('./public/images/berita/'.$row->gallery);
		$this->Berita_model->delete(array('id' => $id));
		$this->session->set_flashdata('success','Record deleted');

	}

}