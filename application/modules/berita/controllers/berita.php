<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berita extends MY_Controller {

	const ITEM_PER_PAGE = 10;

	public function __construct(){
		parent::__construct();
		$this->load->model('Berita_model');
		$this->Berita_model->set_table('news');
	}

	public function index($page=0){
		//list berita, order DESC
		$count_news = $this->Berita_model->fetch_rows();
		
		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'berita/index/';
		$config['total_rows'] = count($count_news);
		$config['per_page'] = self::ITEM_PER_PAGE;
		$config['cur_tag_open'] = '<span class="yellow" style="margin:0 10px">';
		$config['cur_tag_close'] = '</span>';
		$config['next_page'] = '&laquo;';
		$config['prev_page'] = '&raquo;';

		$result = $this->Berita_model->get_berita($page,self::ITEM_PER_PAGE);

		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();

		$data['result'] = $result;
		$data = $this->_common_data($data);
		$this->load->view('berita_index',$data);
	}

	public function detail($year=NULL,$month=NULL,$day=NULL,$name=NULL,$id=NULL){
		//detail berita

		if(!isset($year) || !isset($month) || !isset($day) || !isset($name) || !isset($id)){
			redirect('/');
		}

		$params = $year . '/' . $month . '/' .$day. '/' . $name . '/' . $id . '/';
		$row = $this->Berita_model->fetch_row(NULL,array('id' => $id));
		if(empty($row)){
			redirect('/');
		}

		if($row->link != $params){
			redirect('/');
		}

		$data['detail'] = $row;
		$data = $this->_common_data($data);
		$this->load->view('detail_berita',$data);
	}

	public function comment_post(){
		//posting comment, redirect to detail
		if(!$this->input->post('id')){
			redirect('/');
		}

		$id = $this->input->post('id');
		$detail = $this->Berita_model->fetch_row(NULL,array('id' => $id));

		$request = array(
			'email' => $this->input->post('email'),
			'name' => $this->input->post('name'),
			'website' => $this->input->post('website'),
			'content' => $this->input->post('content'),
			'email_notify' => $this->input->post('email_notify'),
			'table' => 'news'
		);

		$this->Berita_model->set_table('comments');
		$this->Berita_model->insert($request);

		$this->session->set_flashdata('success','Terima kasih. Komentar anda telah tersimpan.');
		redirect('berita/detail/'.$detail->link);
	}
}