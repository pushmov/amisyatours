<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
		if(!$this->session->userdata('admin_amisyatours')){
			$this->session->set_flashdata('error','<strong>Error</strong> Your session has expired.');
			redirect('welcome/admin');
		}
	}

	public function index(){
		$this->Appmodel->set_table('haji');
		$row = $this->Appmodel->fetch_row(NULL, array('name' => 'about'));
		$data['row'] = $row;
		$this->load->view('admin_haji_about',$data);
	}

	public function edit($id=NULL){
		if(!isset($id)){
			redirect('haji/admin');
		}

		$this->Appmodel->set_table('haji');
		$row = $this->Appmodel->fetch_row(NULL, array('id' => $id));
		if(empty($row)){
			redirect('haji/admin');
		}

		switch($row->name){
			
			case 'persyaratan':
				$data['title'] = 'Persyaratan Haji';
				break;
			case 'biaya':
				$data['title'] = 'Biaya Haji';
				break;
			default:
				$data['title'] = 'Mengenai Haji';
				break;
		}

		$data['row'] = $row;
		
		/** get router */
		$this->Appmodel->set_table('app_routes');
		$data['routes'] = $this->Appmodel->fetch_row(NULL,array('module' => 'pages', 'control' => 'haji', 'method' => $row->name));

		$this->load->view('admin_haji_about_edit',$data);
	}

	public function update(){
		
		if(!$this->input->post('name')){
			redirect('haji/admin/');
		}

		$slug = str_replace(base_url(), '', $this->input->post('slug'));


		$sess_data = $this->session->userdata('admin_amisyatours');
		$request = array(
			'content' => $this->input->post('content'),
			'created_by' => $sess_data['id'],
			'date_updated' => date('Y-m-d H:i:s')
			);

		$this->Appmodel->set_table('haji');
		$this->Appmodel->update($request, array('name' => $this->input->post('name')));

		$this->Appmodel->set_table('app_routes');
		$this->Appmodel->update(array('slug' => $slug), array('controller' => $this->input->post('controller')));

		$this->session->set_flashdata('success','<strong>Sukses</strong> Konten berhasil di update');
		redirect('haji/admin');
	}

	public function persyaratan(){
		$this->Appmodel->set_table('haji');
		$row = $this->Appmodel->fetch_row(NULL, array('name' => 'persyaratan'));
		$data['row'] = $row;
		$this->load->view('admin_haji_persyaratan',$data);
	}

	public function biaya(){
		$this->Appmodel->set_table('haji');
		$row = $this->Appmodel->fetch_row(NULL, array('name' => 'biaya'));
		$data['row'] = $row;
		$this->load->view('admin_haji_biaya',$data);
	}


}