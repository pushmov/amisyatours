<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div class="heading" style="margin:10px 0">
				<?php echo $title; ?> - Edit
			</div>


			<div class="clearfix" style="margin:20px 0">
				<?php if($row->date_updated != '') :?>
				<div class="float-left"><h6>Last Update : <?php echo date('D, d/M/Y',strtotime($row->date_updated)); ?></h6></div>
				<?php endif; ?>
			</div>

			<form name="editpage" id="editpage" action="<?php echo base_url();?>haji/admin/update/" method="POST" >
				<input type="hidden" name="name" value="<?php echo $row->name; ?>">
				<textarea style="height:400px" name="content" id="content">
					<?php echo $row->content; ?>
				</textarea>
				<table class="maintable" style="width:100%">
					<tr>
						<td colspan="2"><h4>URL Setting</h4></td>
					</tr>
					<tr>
						<td>System URL : </td>
						<td>
							<?php echo base_url() . $routes->controller;?>
							<input type="hidden" name="controller" value="<?php echo $routes->controller; ?>" >
						</td>

					</tr>
					<tr>
						<td>Custom URL<span class="red">*</span> : </td>
						<td><input type="text" style="width:90% !important" name="slug" id="slug" value="<?php echo base_url() . $routes->slug; ?>"></td>
					</tr>
				</table>
				<div style="margin:10px 0" class="right"><button id="submit" class="btn btn-primary"><i class="icon-edit icon-white"></i>Simpan</button></div>
			</form>
		</div>
</div>
<script>
	$('#submit').click(function(){
		if($('#slug').val() == '')
			return false;
		$('#editpage').submit();
	});
</script>
<?php $this->load->view('admin_footer'); ?>
