<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div class="heading" style="margin:10px 0">
				Persyaratan Haji
			</div>

			<?php if($this->session->flashdata('error')) : ?>
			<div class="alert alert-error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $this->session->flashdata('error'); ?>
			</div>
			<?php endif;?>

			<?php if($this->session->flashdata('success')) : ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $this->session->flashdata('success'); ?>
			</div>
			<?php endif; ?>


			<div class="clearfix">
				<?php if($row->date_updated != '') :?>
				<div class="float-left"><h6>Last Update : <?php echo date('D, d/M/Y H:i:s',strtotime($row->date_updated)); ?></h6></div>
				<?php endif; ?>
				<div class="float-right"><button onclick="window.location='<?php echo base_url(); ?>haji/admin/edit/<?php echo $row->id; ?>'" class="btn"><i class="icon-edit"></i>Edit Content</button></div>
			</div>

			<div class="main radius5" style="border:1px solid #BBBBBB; margin:20px 0;padding:20px;">
				<?php echo $row->content; ?>
			</div>

		</div>

</div>
<?php $this->load->view('admin_footer'); ?>
