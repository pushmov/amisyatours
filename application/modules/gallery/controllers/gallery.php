<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends MY_Controller {

	const DEFAULT_GALLERY = 8;

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
		$this->load->model('Gallery_model');
	}

	public function index(){

		$this->Appmodel->set_table('options');
		$gallery_size = $this->Appmodel->fetch_row(NULL,array('name' => 'gallery'));
		if(!empty($gallery_size)){

			$arrsize = json_decode($gallery_size->value);
			$size = $arrsize->size;

		} else {
			$size = self::DEFAULT_GALLERY;
		}

		$this->Gallery_model->set_table('gallery');
		$gallery = $this->Gallery_model->get_gallery($size);
		
		$data['gallery'] = $gallery;
		$data = $this->_common_data($data);

		$this->load->view('gallery_index',$data);
	}


}