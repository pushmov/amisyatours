<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller {

	const MAX_ALLOWED_SHIPMENT_PICTURE = 5120;
	const FILE_ALLOWED_TYPES = 'jpg|jpeg|bmp|gif|png|JPG|JPEG|BMP|GIF|PNG';

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
		$this->load->model('Gallery_model');
		if(!$this->session->userdata('admin_amisyatours')){
			$this->session->set_flashdata('error','<strong>Error</strong> Your session has expired.');
			redirect('welcome/admin');
		}
	}

	public function index(){

		$this->Gallery_model->set_table('gallery');
		$gallery = $this->Gallery_model->get_gallery();
		$data['gallery'] = $gallery;
		$this->load->view('admin_gallery_index',$data);
	}

	public function upload(){
		
		$response = array();
		
		$photo = (!empty($_FILES['file']['tmp_name'])) ? time() . '-1' : '';

		/* upload files process */		
		$config['allowed_types'] = self::FILE_ALLOWED_TYPES;
		$config['max_size'] = self::MAX_ALLOWED_SHIPMENT_PICTURE;
		$config['upload_path'] = './public/images/superbox/';
		
		$ci = get_instance();
		if($photo != '')
		{
			$config['file_name'] = $photo;
			$ci->load->library('upload',$config);
			if(!$ci->upload->do_upload('file'))
			{
				$this->session->set_flashdata('errorcode','imageerror');
				$response['success'] = 'fail';
				$response['newimage'] = '';
				$response['errorcode'] = 1;
				$response['message'] = 'Allowed picture : png, jpg or bmp only | max size : '.self::FILE_ALLOWED_TYPES.' KB';
				echo json_encode($response);
				exit();
			}
			else
			{
				$data_photo = $ci->upload->data();

				$photo = $data_photo['file_name'];
				$response['success'] = 'success';
				$response['message'] = 'Success. Redirecting ...';

				$sess_data = $this->session->userdata('admin_amisyatours');

				$this->Appmodel->set_table('gallery');

				$inser_data = array(
					'created_by'  => $sess_data['id'],
					'gallery_name' => $photo
				);
				$this->Appmodel->insert($inser_data);

				echo json_encode($response);
				exit();
			}
		}
		
	
	}

	public function delete($id=NULL){
		if(!isset($id)){
			redirect('gallery/admin/');
		}

		$this->Appmodel->set_table('gallery');
		$row = $this->Appmodel->fetch_row(NULL,array('id' => $id));

		if(empty($row)){
			redirect('gallery/admin/');	
		}

		$this->Appmodel->delete(array('id' => $id));
		unlink('./public/images/superbox/'.$row->gallery_name);

		$this->session->set_flashdata('success','<strong>Success.</strong> Record deleted.');
		redirect('gallery/admin/');
	}


}