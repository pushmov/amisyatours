<?php

Class Gallery_model extends Appmodel
{

	function get_gallery($size=NULL){
		if(isset($size)){
			$this->db->limit($size);
		}

		return $this->db->get($this->table)
		->result();

	}

}
