	<?php $this->load->view('header'); ?>
	<?php $this->load->view('plugins/flexnav'); ?>
	<?php $this->load->view('plugins/prettyphoto'); ?>
	<?php $this->load->view('header_end'); ?>
	<?php $this->load->view('statics/menu'); ?>
	


				<div class="content" style="padding: 10px 0 !important">
					<div class="left fs24"><span style="color:#2C9192">Our</span> Gallery :</div>

					<?php if(!empty($gallery)) : ?>
					<div id="gallery" class="gallery">
						<table align="center" style="width:100%">
								<tr>
									<?php $counter = 1; ?>
									<?php foreach($gallery as $row) : ?>
									<td>
										<div class="gallerybox">
											<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/superbox/<?php echo $row->gallery_name; ?>">
												<img src="<?php echo base_url(); ?>public/images/superbox/<?php echo $row->gallery_name; ?>" width="200">	
											</a>
										</div>
									</td>
									<?php if($counter % 4 == 0) : ?></tr><tr><?php endif; ?>
									<?php $counter++; ?>
									<?php endforeach; ?>
								</tr>
						</table>
					</div>
					<?php else : ?>
					<div id="gallery" class="gallery">
						<table align="center" style="width:100%">
								<tr>
									<td>
										<div class="gallerybox">
											<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg">
												<img src="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg" width="200" height="300">	
											</a>
										</div>
									</td>
									<td>
										<div class="gallerybox">
											<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg">
												<img src="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg" width="200" height="300">	
											</a>
										</div>
									</td>
									<td>
										<div class="gallerybox">
											<a href="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg">
												<img src="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg" width="200" height="300">	
											</a>
										</div>
									</td>
									<td>
										<div class="gallerybox">
											<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg">
												<img src="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg" width="200" height="300">	
											</a>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="gallerybox">
											<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg">
												<img src="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg" width="200" height="300">	
											</a>
										</div>
									</td>
									<td>
										<div class="gallerybox">
											<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg">
												<img src="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg" width="200" height="300">	
											</a>
										</div>
									</td>
									<td>
										<div class="gallerybox">
											<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg">
												<img src="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg" width="200" height="300">	
											</a>
										</div>
									</td>
									<td>
										<div class="gallerybox">
											<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg">
												<img src="<?php echo base_url(); ?>public/images/superbox/superbox-full-1.jpg" width="200" height="300">	
											</a>
										</div>
									</td>
								</tr>
						</table>
					</div>
					<?php endif; ?>
				</div>
				</section>
			</div>
				<?php $this->load->view('statics/footer'); ?>
		</div>
	<script>

		$(document).ready(function(){
		    $(".gallerybox a").append("<span></span>");
		    $(".gallerybox a").hover(function(){
		        $(this).children("span").fadeIn(600);
		    },function(){

		        $(this).children("span").fadeOut(200);
		    });


		});
	</script>

	<?php $this->load->view('html_end'); ?>