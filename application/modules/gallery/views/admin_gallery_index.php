<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<div class="clearfix">
				<div class="float-left">Total Records : <?php echo sizeof($gallery); ?></div>
				<div class="float-right">
					<a id="add" href="#ex1" rel="modal:open" class="btn" style="color:#000000 !important;font-weight:400"><span class="icon-plus"></span> Add new gallery</a>
				</div>
			</div>

			<?php if($this->session->flashdata('success')) : ?>
			<div style="margin:10px 0">
				<div class="alert alert-success">
				    <button type="button" class="close" data-dismiss="alert">&times;</button>
				    <?php echo $this->session->flashdata('success'); ?>
				</div>
			</div>
			<?php endif;?>
			
			<div>
				<table class="maintable" style="border-top:5px solid #71C39A">
						<tr>
							<?php $counter = 1; ?>
							<?php foreach($gallery as $row) : ?>
							<td>
								<div class="gallerybox">
									<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/superbox/<?php echo $row->gallery_name; ?>">
										<img src="<?php echo base_url(); ?>public/images/superbox/<?php echo $row->gallery_name; ?>" width="200">	
									</a>
								</div>
								<div style="margin:10px 0;width:100%;"><button onclick="window.location='<?php echo base_url(); ?>gallery/admin/delete/<?php echo $row->id; ?>'" class="btn btn-danger"><i class="icon-remove icon-white"></i>Remove</button></div>
							</td>
							<?php if($counter % 4 == 0) : ?>
							</tr><tr>
							<?php endif; ?>
							<?php $counter++; ?>
							<?php endforeach; ?>
						</tr>
				</table>
			</div>
			<!-- Modal HTML embedded directly into document -->
			<div id="ex1" style="display:none;">
				<h4>Upload Gallery</h4>
				<form id="uploadform" name="uploadform" action="<?php echo base_url(); ?>gallery/admin/upload/" method="POST">
					<table class="maintable" style="border-top:none;">
						<tr>
							<td>
								Select File : 
							</td>
							<td>
								<div style="margin:10px 0"><input type="file" name="file" id="file"></div>
								<div id="notifier" style="display:none">Uploading ...</div>
								<div id="progress-1" class="progress">
									<div class="bar"><span class="percent"></span></div >
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="right">
								<a class="btn btn-primary" style="color:#FFFFFF !important" href="javascript:void(0)" id="upload"><i style="margin-right:5px;" class="icon-upload icon-white"></i>Upload</a>
							</td>
						</tr>
					</table>
				</form>
			</div>

		</div>

</div>
<script>

	$('#upload').click(function(){
		if($('#file').val() == ''){
			return false;
		}

		$(this).removeClass('btn-primary').addClass('btn-danger').html("Processing");

		var bar = $('.bar');
		var percent = $('.percent');
		var status = $('#status');
		$('.progress').show();

		setTimeout(function(){
			$('#uploadform').ajaxSubmit({
				dataType:  'json', 
				url: '<?php echo base_url(); ?>gallery/admin/upload/',
				beforeSend: function() {
					status.empty();
					var percentVal = '0%';
					bar.width(percentVal)
					percent.html(percentVal);
				},
				
				uploadProgress: function(event, position, total, percentComplete) {
					var percentVal = percentComplete + '%';
					bar.width(percentVal)
					percent.html(percentVal);
				},
				
				success: processJson
			});
			return false;
		},3000);

	});

	function processJson(data){
		var bar = $('.bar');
		var percent = $('.percent');
		var status = $('#status');
		
		var percentVal = '100%';
		$('#notifier').html(data.message);
		$(this).removeClass('btn-danger').addClass('btn-primary').html("Upload");
		bar.width(percentVal)
		percent.html(percentVal);
		if(data.success == 'success'){
			window.location="<?php echo base_url(); ?>gallery/admin/"
		} else {
			return false;
		}
	}
</script>
<?php $this->load->view('admin_footer'); ?>
