<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<div class="clearfix">
				<div class="float-left">Social Media Edit</div>
			</div>

			<?php if($this->session->flashdata('success')) : ?>
			<div style="margin:10px 0">
				<div class="alert alert-success">
				    <button type="button" class="close" data-dismiss="alert">&times;</button>
				    <?php echo $this->session->flashdata('success'); ?>
				</div>
			</div>
			<?php endif;?>

			<div>
				<form name="socmed" id="socmed" action="<?php echo base_url(); ?>options/admin/socmed_save/" method="POST">
				<table class="maintable" style="border-top:5px solid #71C39A">
					<thead>
						<tr>
							<th>Tampilkan</th>
							<th>Icon</th>
							<th>Nama</th>
							<th>Target</th>
							<th>Date Created</th>
							<th>Date Updated</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($socmed as $row) : ?>
						<tr>
							<td>
								<input type="checkbox" value="<?php echo ($row->status) ? '1' : '0'; ?>" <?php echo ($row->status) ? 'checked' : ''; ?> id="checkbox-0<?php echo $row->id; ?>" name="<?php echo $row->name.'[status]'; ?>">
							</td>
							<td>
								<a target="_blank" href="<?php echo $row->target; ?>"><img src="<?php echo base_url(); ?>public/images/socmed/colour/<?php echo $row->icon; ?>"></a>
							</td>
							<td><?php echo ucwords($row->name); ?></td>
							<td><input type="text" name="<?php echo $row->name.'[target]'; ?>" value="<?php echo $row->target; ?>"></td>
							<td><?php echo date('D, d/M/Y H:i:s',strtotime($row->date_created)); ?></td>
							<td><?php echo ($row->date_updated != '') ? date('D, d/M/Y H:i:s',strtotime($row->date_updated)) : '-'; ?></td>
						</tr>
						<?php endforeach; ?>
						<tr>
							<td colspan="7" ><button type="submit" name="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i>Simpan</button></td>
						</tr>
					</tbody>
				</table>
				</form>
			</div>
		</div>

</div>
<?php $this->load->view('admin_footer'); ?>
