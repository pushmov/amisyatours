<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<div class="clearfix">
				<div class="float-left">Edit Category</div>
			</div>


			<?php if($this->session->flashdata('success')) : ?>
			<div style="margin:10px 0">
				<div class="alert alert-success">
				    <button type="button" class="close" data-dismiss="alert">&times;</button>
				    <?php echo $this->session->flashdata('success'); ?>
				</div>
			</div>
			<?php endif;?>

			<div>
				<form name="editcat" id="editcat" action="<?php echo base_url(); ?>options/admin/category_edit/<?php echo $detail->category_id; ?>" method="POST">
				<table class="maintable" style="width:60%;border-top:5px solid #71C39A">
					<tbody>
						<tr>
							<td>Name : </td>
							<td>
								<input type="text" name="name" id="name" value="<?php echo $detail->category_display_name; ?>">
							</td>
						</tr>
						<tr>
							<td class="right" colspan="2"><button class="btn btn-primary">Simpan</button></td>
						</tr>
					</tbody>
				</table>
				</form>
			</div>
		</div>

</div>

<?php $this->load->view('admin_footer'); ?>
