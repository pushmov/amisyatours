<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<div class="clearfix">
				<div class="float-left">Category Product</div>
				<div class="float-right"><a rel="modal:open" href="#ex1" style="color:#FFFFFF !important" class="btn btn-primary" ><i class="icon-plus icon-white"></i>Add new item</a></div>
			</div>


			<?php if($this->session->flashdata('success')) : ?>
			<div style="margin:10px 0">
				<div class="alert alert-success">
				    <button type="button" class="close" data-dismiss="alert">&times;</button>
				    <?php echo $this->session->flashdata('success'); ?>
				</div>
			</div>
			<?php endif;?>

			<div>
				<table class="maintable" style="border-top:5px solid #71C39A">
					<thead>
						<tr>
							<th>Nama</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($categories as $c) : ?>
						<tr>
							<td><?php echo $c->category_display_name; ?></td>
							<td class="center">
								<button class="btn" onclick="window.location='<?php echo base_url(); ?>options/admin/category_edit/<?php echo $c->category_id; ?>'"><i class="icon-edit"></i>Edit</button><button onclick="window.location='<?php echo base_url(); ?>options/admin/category_delete/<?php echo $c->category_id; ?>'" style="margin-left:8px" class="btn btn-danger"><i class="icon-remove icon-white"></i>Delete</button></td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>

</div>
<div id="ex1" style="display:none;">
	<h4>Add Category</h4>
	<form id="uploadform" name="uploadform" action="<?php echo base_url(); ?>options/admin/category_add/" method="POST">
		<table class="maintable" style="border-top:none;">
			<tr>
				<td>Name : </td>
				<td>
					<div style="margin:10px 0"><input type="text" name="name" id="name"></div>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="right">
					<button class="btn btn-primary" type="submit" id="add"><i style="margin-right:5px;" class="icon-upload icon-white"></i>Add</button>
				</td>
			</tr>
		</table>
	</form>
</div>
<?php $this->load->view('admin_footer'); ?>
