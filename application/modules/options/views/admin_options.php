<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div class="heading">
				Website Options

			</div>
			<p style="margin:10px 0;font-weight:400">(display item in page)</p>

			<?php if($this->session->flashdata('success')) : ?>
			<div style="margin:10px 0">
				<div class="alert alert-success">
				    <button type="button" class="close" data-dismiss="alert">&times;</button>
				    <?php echo $this->session->flashdata('success'); ?>
				</div>
			</div>
			<?php endif; ?>

			<div class="main">
				<form name="save-options" id="save-options" action="<?php echo base_url(); ?>options/admin/save/" method="POST">
				<table class="maintable" style="width:60%;border-top:5px solid #71C39A">
					<?php foreach($options as $option): ?>
					<tr>
						<td><?php echo $option->name; ?></td>
						<td><input type="text" name="<?php echo $option->name.'['.key($option->pair).']'; ?>" class="item" value="<?php echo $option->pair[key($option->pair)]; ?>"></td>
					</tr>
					<?php endforeach; ?>
					<tr>
						<td class="right" colspan="2"><button type="submit" id="save" class="btn btn-primary" name="submit"><i class="icon-ok icon-white" style="margin-right:5px"></i>Simpan</button></td>
					</tr>
				</table>
				</form>
			</div>

		</div>

</div>
<script>
	$('#save').click(function(){
		var check = true;
		$('.item').each(function(){
			if($(this).val() == ''){
				check = false;
				return false;	
			}
		});

		if(!check){
		return false;
		}
	});
</script>
<?php $this->load->view('admin_footer'); ?>
