<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller {

	const MAX_ALLOWED_SHIPMENT_PICTURE = 5120;
	const FILE_ALLOWED_TYPES = 'jpg|jpeg|bmp|gif|png|JPG|JPEG|BMP|GIF|PNG';

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
		$this->Appmodel->set_table('options');
		if(!$this->session->userdata('admin_amisyatours')){
			$this->session->set_flashdata('error','<strong>Error</strong> Your session has expired.');
			redirect('welcome/admin');
		}
	}

	public function index(){

		$options = $this->Appmodel->fetch_rows();
		foreach ($options as $value) {
			$vals = json_decode($value->value);
			$value->pair = (array) $vals;
		}


		$data['options'] = $options;
		$this->load->view('admin_options',$data);
	}

	public function save(){
		
		foreach($_POST as $key => $value){
			if(!empty($value)){
				$type = $key;
				$json_value = json_encode($value);

				$this->Appmodel->update(array('value' => $json_value), array('name' => $type));
			}
		}

		$this->session->set_flashdata('success','<strong>Success.</strong> Data berhasi disimpan.');
		redirect('options/admin');

	}

	public function socmed(){

		$this->Appmodel->set_table('social_media');
		$socmed = $this->Appmodel->fetch_rows();

		$data['socmed'] = $socmed;
		$this->load->view('admin_options_socmed',$data);
	}

	public function socmed_save(){
		
		foreach($_POST as $key => $val){
			if(!empty($val)){
				
				$update_arr = array(
					'date_updated' => date('Y-m-d H:i:s'),
					'target' => $val['target'],
					'status' => isset($val['status']) ? 1 : 0,
					);
				$this->Appmodel->set_table('social_media');
				$this->Appmodel->update($update_arr, array('name' => $key));
			}
			
		}

		$this->session->set_flashdata('success','<strong>Success.</strong> Data berhasil diupdate');
		redirect('options/admin/socmed/');

	}

	public function category(){
		$this->Appmodel->set_table('categories');
		$data['categories'] = $this->Appmodel->fetch_rows();

		$this->load->view('admin_options_category',$data);
	}

	public function category_add(){

		$this->Appmodel->set_table('categories');
		if($this->input->post('name')){

			$name = $this->input->post('name');
			$request = array(
				'category_display_name' => $name,
				'category_url_name' => strtolower(str_replace(' ', '-', $name))
			);

			$this->Appmodel->insert($request);
			$this->session->set_flashdata('success','<strong>Success.</strong> Data berhasil di simpan');
			redirect('options/admin/category');
		}

	}

	public function category_edit($id=NULL){
		
		if(!isset($id)){
			redirect('options/admin/category/');
		}
		$this->Appmodel->set_table('categories');

		if($this->input->post('name')){

			$name = $this->input->post('name');
			$request = array(
				'category_display_name' => $name,
				'category_url_name' => strtolower(str_replace(' ', '-', $name))
			);

			$this->Appmodel->update($request, array('category_id' => $id));
			$this->session->set_flashdata('success','<strong>Success.</strong> Data berhasil di update');
			redirect('options/admin/category');
		}

		
		$data['detail'] = $this->Appmodel->fetch_row(NULL,array('category_id' => $id));

		$this->load->view('admin_options_category_edit',$data);
	}

	public function category_delete($id){
		
		$this->Appmodel->set_table('categories');
		$this->Appmodel->delete(array('category_id' => $id));

		$this->session->set_flashdata('success','<strong>Success.</strong> Data berhasil di remove');
		redirect('options/admin/category/');
	}

}