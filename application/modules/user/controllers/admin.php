<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Appmodel');
		if(!$this->session->userdata('admin_amisyatours')){
			$this->session->set_flashdata('error','<strong>Error</strong> Your session has expired.');
			redirect('welcome/admin');
		}
	}

	public function index(){

		$this->Appmodel->set_table('users');
		$result = $this->Appmodel->fetch_rows();

		$data['members'] = $result;
		$this->load->view('admin_user',$data);
	}

	public function add(){
		$this->load->view('admin_user_add');
	}

	public function edit($id=NULL){
		if(!isset($id)){
			redirect('user/admin');
		}

		$this->Appmodel->set_table('users');
		$row = $this->Appmodel->fetch_row(NULL,array('id' => $id));
		if(empty($row)){
			redirect('user/admin');
		}

		$data['user'] = $row;
		$this->load->view('admin_user_edit',$data);
	}

	public function delete($id=NULL){
		if(!isset($id)){
			redirect('user/admin');
		}

		$this->Appmodel->set_table('users');
		$row = $this->Appmodel->fetch_row(NULL,array('id' => $id));
		if(empty($row)){
			redirect('user/admin');
		}


		$this->Appmodel->delete(array('id' => $id));
		unlink('./public/images/profpic/' . $row->profpic);
		$this->session->set_flashdata('success','<strong>Success!</strong> Record successfully deleted.');
		redirect('user/admin');

	}

	public function search($params=NULL){

	}


}