<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<h2>Edit member</h2>

			<div id="ajax-response">
				<table class="maintable" style="width:80%;border-top:4px solid #71C39A">
					<tr>
						<td>Email<span class="red">*</span></td>
						<td><input type="text" name="email" id="email" value="<?php echo $user->email; ?>"></td>
					</tr>
					<tr>
						<td>Username<span class="red">*</span></td>
						<td><input ty	pe="text" name="username" id="username" value="<?php echo $user->username; ?>"></td>
					</tr>
					<tr>
						<td>Password<span class="red">*</span></td>
						<td><input type="password" name="password" id="password" value="<?php echo $user->password; ?>"></td>
					</tr>
					<tr>
						<td>Confirm Password<span class="red">*</span></td>
						<td><input type="password" name="confirmpassword" id="confirmpassword" value="<?php echo $user->password; ?>"></td>
					</tr>
					<tr>
						<td>First Name</td>
						<td><input type="text" name="fname" id="fname" value="<?php echo $user->fname; ?>"></td>
					</tr>
					<tr>
						<td>Last Name</td>
						<td><input type="text" name="lname" id="lname" value="<?php echo $user->lname; ?>"></td>
					</tr>
					<tr>
						<td>Level</td>
						<td>
							<select name="level" id="level">
								<option value="1">User</option>
								<option value="0">Admin</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Activation Status</td>
						<td>
							<select name="active" id="active">
								<option value="1">Active</option>
								<option value="0">Non Active</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Profile Picture</td>
						<td>
							<?php if($user->profpic == '') : ?>
							<input type="file" name="profpic" id="profpic" >
							<?php else : ?>
							<img src="<?php echo base_url(); ?>public/images/profpic/<?php $user->profpic; ?>">
							<?php endif; ?>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="left"><button class="btn btn-primary"><i class="icon-user icon-white"></i>Update</button></td>
					</tr>
				</table>
				<div>Field dengan mark (<span class="red">*</span>) wajib diisi</div>
			</div>
		</div>

</div>
<script>
	$(document).ready(function(){
		$('#active').val('<?php echo $user->is_active; ?>');
		$('#level').val('<?php echo $user->level; ?>');
	})
</script>
<?php $this->load->view('admin_footer'); ?>
