<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<h2>Create new member</h2>

			<div id="ajax-response">
				<table class="maintable" style="width:80%;border-top:4px solid #71C39A">
					<tr>
						<td>Email<span class="red">*</span></td>
						<td><input type="text" name="email" id="email" ></td>
					</tr>
					<tr>
						<td>Username<span class="red">*</span></td>
						<td><input type="text" name="username" id="username" ></td>
					</tr>
					<tr>
						<td>Password<span class="red">*</span></td>
						<td><input type="password" name="password" id="password" ></td>
					</tr>
					<tr>
						<td>Confirm Password<span class="red">*</span></td>
						<td><input type="password" name="confirmpassword" id="confirmpassword" ></td>
					</tr>
					<tr>
						<td>First Name</td>
						<td><input type="text" name="fname" id="fname" ></td>
					</tr>
					<tr>
						<td>Last Name</td>
						<td><input type="text" name="lname" id="lname" ></td>
					</tr>
					<tr>
						<td>Level</td>
						<td>
							<select name="level">
								<option value="1">User</option>
								<option value="0">Admin</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Activation Status</td>
						<td>
							<select name="active">
								<option value="1">Active</option>
								<option value="0">Non Active</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Profile Picture</td>
						<td><input type="file" name="profpic" id="profpic" ></td>
					</tr>
					<tr>
						<td colspan="2" class="left"><button class="btn btn-primary"><i class="icon-user icon-white"></i>Create</button></td>
					</tr>
				</table>
				<div>Field dengan mark (<span class="red">*</span>) wajib diisi</div>
			</div>
		</div>

</div>
<script>
	function deleteUser(id){
		var c = confirm('Are you sure want to delete?');
		if(c){
			window.location="<?php echo base_url(); ?>user/admin/delete/"+id;
		}
		return c;
	}
</script>
<?php $this->load->view('admin_footer'); ?>
