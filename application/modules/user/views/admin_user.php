<?php $this->load->view('admin_header'); ?>
	<div class="full clearfix">
		
		<?php $this->load->view('admin_top_panel'); ?>
		<?php $this->load->view('admin_left_panel'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>dashboard/admin/">Home</a>
			</div>
			
			<h2>Member List</h2>

			<?php if($this->session->flashdata('success')) : ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $this->session->flashdata('success'); ?>
			</div>
			<?php endif; ?>

			<?php if($this->session->flashdata('error')) : ?>
			<div class="alert alert-error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $this->session->flashdata('error'); ?>
			</div>
			<?php endif; ?>

			<div class="clearfix">
				<div class="float-left">
					<button onclick="window.location='<?php echo base_url(); ?>user/admin/add/'" class="btn" style="color:#000000 !important" ><i class="icon-plus" style="margin-right:5px;"></i>Add new member</button>
				</div>
			</div>

			<div id="ajax-response">
				<table class="maintable" style="width:60%">
					<thead>
						<tr>
							<th>Member Email</th>
							<th>Level</th>
							<th>Username</th>
							<th>Nama Depan</th>
							<th>Nama Belakang</th>
							<th>Dibuat</th>
							<th>Diupdate</th>
							<th>Status</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>

						<?php if(!empty($members)) : ?>
						<?php foreach($members as $member) : ?>
						<tr>
							<td><?php echo $member->email; ?></td>
							<td><?php echo ($member->level) ? 'user' : 'admin'; ?></td>
							<td><?php echo $member->username; ?></td>
							<td><?php echo $member->fname; ?></td>
							<td><?php echo $member->lname; ?></td>
							<td><?php echo date('D d/M/Y',strtotime($member->date_created)); ?></td>
							<td><?php echo ($member->date_updated != '') ? date('D d/M/Y',strtotime($member->date_created)) : '-'; ?></td>
							<td>
								<?php if($member->is_active) : ?>
								<span style="color:#00FF00">Aktif</span>
								<?php else : ?>
								<span style="color:#FF0000">Non Aktif</span>
								<?php endif; ?>
							</td>
							<td>
								<div class="left" style="width:100px;margin:5px 0">
									<button onclick="window.location='<?php echo base_url(); ?>user/admin/edit/<?php echo $member->id; ?>/'" style="color:#000000 !important" class="btn"><i class="icon-edit" style="margin-right:5px;"></i>Edit</button>
								</div>
								<div class="left" style="width:100px;margin:5px 0">
									<button onclick="return deleteUser('<?php echo $member->id; ?>')" style="color:#FFFFFF !important" class="btn btn-danger"><i class="icon-remove icon-white" style="margin-right:5px;"></i>Delete</button>
								</div>
							</td>
						</tr>
						<?php endforeach; ?>
						<?php else : ?>
						<tr>
							<td colspan="9">No records found</td>
						</tr>
						<?php endif; ?>		
					</tbody>
				</table>
				<div>Total records : <?php echo sizeof($members); ?></div>
			</div>
		</div>

</div>
<script>
	function deleteUser(id){
		var c = confirm('Are you sure want to delete?');
		if(c){
			window.location="<?php echo base_url(); ?>user/admin/delete/"+id;
		}
		return c;
	}
</script>
<?php $this->load->view('admin_footer'); ?>
