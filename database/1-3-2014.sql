-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 03, 2014 at 02:35 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `amisyatours`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_routes`
--

CREATE TABLE IF NOT EXISTS `app_routes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `slug` varchar(192) NOT NULL,
  `controller` varchar(64) NOT NULL,
  `module` varchar(64) NOT NULL,
  `control` varchar(64) NOT NULL,
  `method` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `app_routes`
--

INSERT INTO `app_routes` (`id`, `slug`, `controller`, `module`, `control`, `method`) VALUES
(1, 'haji-plus/mengenai-haji', 'pages/haji/about', 'pages', 'haji', 'about'),
(2, 'haji-plus/persyaratan-ketentuan-haji', 'pages/haji/persyaratan', 'pages', 'haji', 'persyaratan'),
(3, 'haji-plus/biaya-haji', 'pages/haji/biaya', 'pages', 'haji', 'biaya'),
(4, 'umrah-plus/mengenai-umrah', 'pages/umrah/about', 'pages', 'umrah', 'about'),
(5, 'umrah-plus/persyaratan-ketentuan-umrah', 'pages/umrah/persyaratan', 'pages', 'umrah', 'persyaratan'),
(6, 'umrah-plus/biaya-umrah', 'pages/umrah/biaya', 'pages', 'umrah', 'biaya');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_category_id` int(20) NOT NULL,
  `category_display_name` varchar(255) NOT NULL,
  `category_url_name` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `parent_category_id`, `category_display_name`, `category_url_name`) VALUES
(1, 0, 'Mukena', 'mukena'),
(2, 0, 'Pakaian Ihrom', 'pakaian-ihrom'),
(3, 0, 'Accessories', 'accessories'),
(4, 0, 'Oleh-oleh haji dan umroh', 'oleh-oleh');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `content` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email_notify` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `comments`
--


-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `gallery_name` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `gallery_name`, `date_created`, `created_by`) VALUES
(1, 'superbox-full-1.jpg', '2013-12-26 19:18:43', 1),
(2, 'superbox-full-2.jpg', '2013-12-26 19:18:43', 1),
(3, 'superbox-full-3.jpg', '2013-12-26 19:18:43', 1),
(4, 'superbox-full-4.jpg', '2013-12-26 19:18:43', 1),
(5, 'superbox-full-5.jpg', '2013-12-26 19:18:43', 1),
(6, 'superbox-full-6.jpg', '2013-12-26 19:18:43', 1),
(7, 'superbox-full-7.jpg', '2013-12-26 19:18:43', 1),
(8, 'superbox-full-8.jpg', '2013-12-26 19:18:43', 1),
(10, '1388062989-1.jpg', '2013-12-26 20:03:09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `haji`
--

CREATE TABLE IF NOT EXISTS `haji` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `created_by` int(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `haji`
--

INSERT INTO `haji` (`id`, `name`, `content`, `created_by`, `date_created`, `date_updated`) VALUES
(1, 'about', '<h3 style="text-align: justify;"><span style="color: #000000;">Pengertian Haji</span></h3>\r\n<p><img src="http://amisyatours.com/images/stories/Makkah_by_TKD_Queen.jpg" alt="" border="0" /></p>\r\n<p style="text-align: justify;"><span style="color: #000000;"><strong>Haji</strong> adalah salah satu rukun Islam yang lima. Menunaikan ibadah haji adalah bentuk ritual tahunan bagi kaum muslim yang mampu secara material, fisik, maupun keilmuan dengan berkunjung ke beberapa tempat di Arab Saudi dan melaksanakan beberapa kegiatan pada satu waktu yang telah ditentukan yaitu pada bulan Dzulhijjah.</span></p>\r\n<p style="text-align: justify;"><span style="color: #000000;">Secara estimologi (bahasa), Haji berarti niat (Al Qasdu), sedangkan menurut syara` berarti Niat menuju Baitul Haram dengan amal-amal yang khusus.Temat-tempat tertentu yang dimaksud dalam definisi diatas adalah selain Ka`bah dan Mas`a (tempat sa`i), juga Padang Arafah (tempat wukuf), Muzdalifah (tempat mabit), dan Mina (tempat melontar jumroh).<br /> Sedangkan yang dimaksud dengan waktu tertentu adalah bulan-bulan haji yaitu dimulai dari Syawal sampai sepuluh hari pertama bulan Dzulhijjah. Amalan ibadah tertentu ialah thawaf, sa`i, wukuf, mazbit di Muzdalifah, melontar jumroh, dan mabit di Mina.</span></p>\r\n<h3 style="text-align: justify;"><span style="color: #000000;">Jenis-jenis Haji</span></h3>\r\n<p style="text-align: justify;"><span style="color: #000000;"><strong>Haji Ifrad, artinya menyendiri</strong><br /> Pelaksanaan ibadah haji disebut ifrad jika sesorang melaksanakan ibadah haji dan umroh dilaksanakan secara sendiri-sendiri, dengan mendahulukan ibadah haji. Artinya, ketika calon jamaah haji mengenakan pakaian ihram di miqat-nya, hanya berniat melaksanakan ibadah haji. Jika ibadah hajinya sudah selesai, maka orang tersebut mengenakan ihram kembali untuk melaksanakan ibadah umroh.</span></p>\r\n<p style="text-align: justify;"><span style="color: #000000;"><strong>Haji Tamattu`, artinya bersenang-senang</strong><br /> Pelaksanaan ibadah haji disebut Tamattu` jika seseorang melaksanakan ibadah umroh dan Haji di bulan haji yang sama dengan mendahulukan ibadah Umroh. Artinya, ketika seseorang mengenakan pakaian ihram di miqat-nya, hanya berniat melaksanakan ibadah Umroh. Jika ibadah Umrohnya sudah selesai, maka orang tersebut mengenakan ihram kembali untuk melaksanakan ibadah Haji.<br /> Tamattu` dapat juga berarti melaksanakan ibadah Umroh dan Haji didalam bulan-bulan serta didalam tahun yang sama, tanpa terlebih dahulu pulang ke negeri asal.</span></p>\r\n<p style="text-align: justify;"><span style="color: #000000;"><strong>Haji Qiran, artinya menggabungkan</strong><br /> Pelaksanaan ibadah Haji disebut Qiran jika seseorang melaksanakan ibadah Haji dan Umroh disatukan atau menyekaliguskan berihram untuk melaksanakan ibadah haji dan umrah. Haji Qiran dilakukan dengan tetap berpakaian ihram sejak miqat makani dan melaksanakan semua rukun dan wajib haji sampai selesai, meskipun mungkin akan memakan waktu lama.</span></p>\r\n<h3 style="text-align: justify;"><span style="color: #000000;">Rukun dan Wajib Haji</span></h3>\r\n<p style="text-align: justify;"><span style="color: #000000;"><strong>Rukun Haji :</strong></span></p>\r\n<ol style="text-align: justify;">\r\n<li><span style="color: #000000;">Ihram</span></li>\r\n<li><span style="color: #000000;">Thawaf Ziyarah (disebut juga dengan Thawaf Ifadhah)</span></li>\r\n<li><span style="color: #000000;">Sa`ie</span></li>\r\n<li><span style="color: #000000;">Wuquf di padang Arafah</span></li>\r\n</ol>\r\n<p style="text-align: justify;"><span style="color: #000000;">Apabila salah satu rukun haji di atas tidak dilaksanakan maka hajinya batal. Sedangkan Abu Hanifah berpendapat bahwa rukun haji hanya ada 2 yaitu: Wuquf dan Thawaf. Ihram dan Sa`I tidak dimasukkan ke dalam rukun karena menurut beliau, ihram adalah syarat sah haji dan sa`I adalah yang wajib dilakukan dalam haji (wajib haji). Sementara Imam syafi`ie berpendapat bahwa rukun haji ada 6 yaitu: Ihram, Thawaf, Sa`ie, Wuquf, Mencukur rambut, dan Tertib berurutan).(Kitabul Fiqh Ala Madzhabil Arba`ah 1/578).</span></p>\r\n<p style="text-align: justify;"><span style="color: #000000;"><strong>Wajib Haji</strong></span></p>\r\n<ol style="text-align: justify;">\r\n<li><span style="color: #000000;">Iharam dimulai dari miqat yang telah ditentukan</span></li>\r\n<li><span style="color: #000000;">Wuquf di Arafah sampai matahari tenggelam</span></li>\r\n<li><span style="color: #000000;">Mabit di Mina</span></li>\r\n<li><span style="color: #000000;">Mabit di Muzdalifah hingga lewat setengah malam</span></li>\r\n<li><span style="color: #000000;">Melempar jumrah</span></li>\r\n<li><span style="color: #000000;">Mencukur rambut</span></li>\r\n<li><span style="color: #000000;">Tawaf Wada`</span></li>\r\n</ol>\r\n<h3 style="text-align: justify;"><span style="color: #000000;">Syarat-syarat Wajib Haji</span></h3>\r\n<ol style="text-align: justify;">\r\n<li><span style="color: #000000;">Islam</span></li>\r\n<li><span style="color: #000000;">Berakal</span></li>\r\n<li><span style="color: #000000;">Baligh</span></li>\r\n<li><span style="color: #000000;">Mampu</span></li>\r\n</ol>\r\n<p style="text-align: justify;"><span style="color: #000000;"><strong>Mewakilkan Seseorang Untuk Berhaji</strong><br /> Tidak boleh bagi seseorang berhaji untuk orang lain kecuali setelah ia berhaji untuk dirinya sendiri. Rasulullah bersabda: Berhajilah untuk dirimu sendiri, kemudian engkau berhaji untuknya.</span></p>\r\n<p style="text-align: justify;"><span style="color: #000000;"><strong>Haji Bagi Anak-anak yang belum Baligh</strong><br /> Tidaklah wajib bagi anak-anak untuk berhaji kecuali ia telah baligh. Namun jika ia telah berhaji maka hajinya sah sebagaimana yang telah diriwayatkan Ibnu Abbas ra bahwa Rasulullah r berjumpa dengan seorang berkendaraan dikawasan Ar-Rauha beliau bersabda: Siapakah kalian? Mereka menjawab: Kami orang-orang muslim, mereka balik bertanya: Siapa anda? Beliau menjawab: Saya Rasul Allah. Lalu ada seorang anak gadis yang masih kecil bertanya: Apakh ini yang disebut haji? Beliau menjawab: Ya dan bagimu pahala (HR. Ahmad, Muslim, Abu Daud, dan An Nasa dishahihkan oleh At Tirmidzi).</span></p>\r\n<h3 style="text-align: justify;"><span style="color: #000000;">Rangkaian Ibadah Haji <br /></span></h3>\r\n<ol style="text-align: justify;">\r\n<li><span style="color: #000000;">Sebelum tanggal 8 Dzulhijjah, calon jamaah haji mulai berbondong untuk melaksanakan Tawaf Haji di Masjid Al Haram, Makkah.</span></li>\r\n<li><span style="color: #000000;">Calon jamaah haji memakai pakaian Ihram (dua lembar kain tanpa jahitan sebagai pakaian haji), sesuai miqatnya, kemudian berniat haji, dan membaca bacaan Talbiyah, yaitu mengucapkan Labbaikallahumma labbaik labbaika laa syarika laka labbaik. Innal hamda wan ni`mata laka wal mulk laa syarika laka..</span></li>\r\n<li><span style="color: #000000;">Tanggal 9 Dzulhijjah, pagi harinya semua calon jamaah haji menuju ke padang Arafah untuk menjalankan ibadah wukuf. Kemudian jamaah melaksanakan ibadah Wukuf, yaitu berdiam diri dan berdoa di padang Arafah hingga Maghrib datang.</span></li>\r\n<li><span style="color: #000000;">Tanggal 9 Dzulhijjah malam, jamaah menuju ke Muzdalifah untuk mabbit (bermalam) dan mengambil batu untuk melontar jumroh secukupnya.</span></li>\r\n<li><span style="color: #000000;">Tanggal 9 Dzulhijjah tengah malam (setelah mabbit) jamaah meneruskan perjalanan ke Mina untuk melaksanakan ibadah melontar Jumroh</span></li>\r\n<li><span style="color: #000000;">Tanggal 10 Dzulhijjah, jamaah melaksanakan ibadah melempar Jumroh sebanyak tujuh kali ke Jumroh Aqobah sebagai simbolisasi mengusir setan. Dilanjutkan dengan tahalul yaitu mencukur rambut atau sebagian rambut.</span></li>\r\n<li><span style="color: #000000;">Jika jamaah mengambil nafar awal maka dapat dilanjutkan perjalanan ke Masjidil Haram untuk Tawaf Haji (menyelesaikan Haji)</span></li>\r\n<li><span style="color: #000000;">Sedangkan jika mengambil nafar akhir jamaah tetap tinggal di Mina dan dilanjutkan dengan melontar jumroh sambungan (Ula dan Wustha).</span></li>\r\n<li><span style="color: #000000;">Tanggal 11 Dzulhijjah, melempar jumrah sambungan (Ula) di tugu pertama, tugu kedua, dan tugu ketiga.</span></li>\r\n<li><span style="color: #000000;">Tanggal 12 Dzulhijjah, melempar jumrah sambungan (Ula) di tugu pertama, tugu kedua, dan tugu ketiga.</span></li>\r\n<li><span style="color: #000000;">Jamaah haji kembali ke Makkah untuk melaksanakan Thawaf Wada` (Thawaf perpisahan) sebelum pulang ke negara masing-masing</span></li>\r\n</ol>\r\n<h3 style="text-align: justify;"><span style="color: #000000;">Lokasi Utama Ibadah Haji<br /></span></h3>\r\n<p style="text-align: justify;"><span style="color: #000000;"><strong>Makkah Al Mukaromah</strong><br /> Di kota Makkah Al-Mukaromah inilah terdapat Masjidil Haram yang didalamnya terdapat Ka`bah yang merupakan kiblat ibadah umat Islam sedunia. Dalam rangkaian perjalanan ibadah haji, Makkah menjadi tempat pembuka dan penutup ibadah haji.</span></p>\r\n<p style="text-align: justify;"><span style="color: #000000;"><strong>Padang Arafah</strong><br /> Padang Arafah terdapat di sebelah timur Kota Makkah. Padang Arafah dikenal sebagai tempat pusatnya haji, sebagai tempat pelaksanaan ibadah wukuf yang merupakan rukun haji. Di Padang Arafah juga terdapat Jabal Rahmah tempat pertama kali pertemuan Nabi Adam dan Hawa. Di luar musim haji, daerah ini tidak dipakai.</span></p>\r\n<p style="text-align: justify;"><span style="color: #000000;"><strong>Kota Muzdalifah</strong><br /> Kota ini tidak jauh dari kota Mina dan Arafah Mota Muzdalifah merupakan tempat jamaah calon haji melakukan Mabit (bermalam) dan mengambil batu untuk melontar Jumroh di Kota Mina.</span></p>\r\n<p style="text-align: justify;"><span style="color: #000000;"><strong>Kota Mina</strong><br /> Kota Mina merupakan tempat berdirinya tugu (jumrah), yaitu tempat pelaksanaan melontarkan batu ke tugu (jumrah) sebagai simbolisasi tindakan nabi Ibrahim ketika mengusir setan. Disana terdapat tiga jumrah yaitu jumrah Aqabah, Jumrah Ula, dan Jumrah Wustha.</span></p>', 1, '2013-12-25 11:59:11', '2013-12-25 13:51:49'),
(2, 'persyaratan', '<h2>Ketentuan &amp; Syarat Haji</h2>\r\n<p><span style="color: #000000;"><strong>PERSYARATAN PENDAFTARAN HAJI KHUSUS</strong></span></p>\r\n<p><span style="color: #000000;">1. Mengisi formulir pendaftaran</span></p>\r\n<p><span style="color: #000000;">2. Setoran awal USD 4,500.00/orang untuk mendapatkan porsi Haji Khusus</span></p>\r\n<p><span style="color: #000000;">3. Foto copy paspor, dengan ketentuan paspor yang masih berlaku minimal 7 bulan dan nama di paspor terdiri dari minimal tiga kata : Muhammad Ali Akbar</span></p>\r\n<p><span style="color: #000000;">4. Foto copy KTP, Kartu Keluarga, Buku Nikah, Akte Kelahiran.</span></p>\r\n<p><span style="color: #000000;">5. Photo terbaru berwarna ukuran :</span></p>\r\n<p><span style="color: #000000;">3 x 4 = 40 lembar</span></p>\r\n<p><span style="color: #000000;">4 x 6 = 10 lembar</span></p>\r\n<p><span style="color: #000000;">Persyaratan photo :</span></p>\r\n<p><span style="color: #000000;">- Ukuran muka close up 80%, background putih</span></p>\r\n<p><span style="color: #000000;">- Tidak menggunakan pakaian dinas, tidak berkacamata dan bagi wanita memakai kerudung/jilbab</span></p>\r\n<p><span style="color: #000000;">6. Surat Keterangan Sehat dari Dokter</span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;"><strong>Biaya Sudah Termasuk :</strong></span></p>\r\n<p><span style="color: #000000;">- Tiket kelas ekonomi pulang pergi CGK &ndash;JED &ndash; CGK</span></p>\r\n<p><span style="color: #000000;">- Pengurusan dokumen visa dan surat kelengkapan keberangkatan</span></p>\r\n<p><span style="color: #000000;">- Pembimbing ibadah, Guide dan Muthowwif yang berpengalaman</span></p>\r\n<p><span style="color: #000000;">- Hotel dan akomodasi sesuai program</span></p>\r\n<p><span style="color: #000000;">- Makan 3 x sehari masakan Indonesia (selama di Saudi)</span></p>\r\n<p><span style="color: #000000;">- Bimbingan manasik sebelum berangkat</span></p>\r\n<p><span style="color: #000000;">- Perlengkapan ibadah Haji</span></p>\r\n<p><span style="color: #000000;">- Buku Kesehatan Haji, termasuk suntik Maningitis</span></p>\r\n<p><span style="color: #000000;">- Ziarah-ziarah dengan transportasi Bus AC</span></p>\r\n<p><span style="color: #000000;">- Air Zam-zam @5 liter</span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;"><strong>Biaya Belum Termasuk :</strong></span></p>\r\n<p><span style="color: #000000;">- Pembuatan paspor dan dokumen lainnya</span></p>\r\n<p><span style="color: #000000;">- DAM (Haji Tamattu) sebesar USD 100.00/orang</span></p>\r\n<p><span style="color: #000000;">- Pemeriksaan kesehatan laboratorium lengkap (General Check Up) dan Vaksin Influenza</span></p>\r\n<p><span style="color: #000000;">- Laundry, telepon dan lain-lain bersifat pribadi</span></p>\r\n<p><span style="color: #000000;">- Biaya tambah nama Rp. 150.000,- (bagi yang nama di paspor kurang dari 3 kata)</span></p>\r\n<p><span style="color: #000000;">- Biaya kursi roda + muthawwif khusus</span></p>\r\n<p><span style="color: #000000;">- Kelebihan bagasi</span></p>', 1, '2013-12-25 12:29:24', '2013-12-25 12:36:06'),
(3, 'biaya', '<table border="2" cellspacing="2" cellpadding="2" align="center">\r\n<tbody>\r\n<tr>\r\n<td style="text-align: center;" colspan="7" width="759" height="34"><strong><span style="color: #000000;"><span style="font-size: large;">HARGA HAJI 2014</span></span></strong></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4" height="26"><span style="color: #000000;"><strong>PROGRAM</strong></span></td>\r\n<td><span style="color: #000000;"><strong>PLATINUM</strong></span></td>\r\n<td><span style="color: #000000;"><strong>GOLDEN</strong></span></td>\r\n<td><span style="color: #000000;"><strong>SILVER</strong></span></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4" height="26"><span style="color: #000000;"><strong>HARGA</strong></span></td>\r\n<td><span style="color: #000000;">US $ 9,500</span></td>\r\n<td><span style="color: #000000;">US $ 9,000</span></td>\r\n<td><span style="color: #000000;">US $ 8,500</span></td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: left;" colspan="4" height="26"><span style="color: #000000;"><strong>KAPASITAS</strong></span></td>\r\n<td><span style="color: #000000;">2 orang/kamar</span></td>\r\n<td><span style="color: #000000;">3 orang/kamar</span></td>\r\n<td><span style="color: #000000;">4 orang/kamar</span></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4" height="26"><span style="color: #000000;"><strong>MEKKAH</strong></span></td>\r\n<td colspan="3"><span style="color: #000000;">Grand zam-zam / Movenpick / setaraf *5</span></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4" height="26"><span style="color: #000000;"><strong>MADINAH</strong></span></td>\r\n<td colspan="3"><span style="color: #000000;">Movenpick /Royal Dyar setaraf *5</span></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4" height="26"><span style="color: #000000;"><strong>JEDDAH</strong></span></td>\r\n<td colspan="3"><span style="color: #000000;">Sofitel Al Hamra / Red Sea / setaraf *5</span></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4" height="26"><span style="color: #000000;"><strong>ARAFAH/MINA</strong></span></td>\r\n<td colspan="3"><span style="color: #000000;">Tenda Ber-AC</span></td>\r\n</tr>\r\n<tr>\r\n<td colspan="7" width="759" height="40"><strong><span style="color: #000000;">NOTE : Harga menyesuaikan tahun keberangkatan dan dapat berubah sewaktu-waktu menyesuaikan perubahan harga hotel di Makkah dan Madinah</span></strong></td>\r\n</tr>\r\n</tbody>\r\n</table>', 1, '2013-12-25 12:37:18', '2013-12-25 12:38:31');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `message_id` int(20) NOT NULL AUTO_INCREMENT,
  `sent_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message_text` text,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`message_id`, `sent_datetime`, `message_text`, `email`, `name`) VALUES
(1, '2013-12-27 19:35:54', 'iasusaisaasuisau', 'email', 'nama');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `name`, `value`) VALUES
(1, 'gallery', '{"size":"9"}'),
(2, 'product', '{"size":"8"}');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `posted_by` int(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL,
  `links` varchar(255) DEFAULT NULL,
  `price` varchar(64) DEFAULT NULL,
  `product_gallery` varchar(255) DEFAULT NULL,
  `category_id` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `posted_by`, `date_created`, `date_updated`, `links`, `price`, `product_gallery`, `category_id`) VALUES
(1, 'Decore 1', 'Decore 1 desc', 1, '2013-12-27 17:16:33', NULL, '2013/12/27/decore-1/1/', '$4,000', 'superbox-full-1.jpg', 0),
(2, 'Decore 2', 'Memang masakan rendang begitu khas karena menggunakan berbagai bumbu khas Indonesia. Masakan rendang berbahan dasar ayam atau daging sapi yang kemudian dicampur dengan berbagai bumbu khas Indonesia semisal lengkuas, serai, cabai, bawang dan berbagai bumbu lainnya.', 1, '2013-12-27 17:19:22', NULL, '2013/12/27/decore-2/2/', '$4,000', 'superbox-full-2.jpg', 0),
(3, 'Decore 3', 'Decore 3 d', 1, '2013-12-27 17:19:22', NULL, '2013/12/27/decore-3/3/', '$4,000', 'superbox-full-3.jpg', 0),
(4, 'Decore 4', 'Decore 4 d', 1, '2013-12-27 17:19:22', NULL, '2013/12/27/decore-4/4/', '$4,000', 'superbox-full-4.jpg', 0),
(5, 'Decore 5', 'Decore 5 d', 1, '2013-12-27 17:19:22', NULL, '2013/12/27/decore-5/5/', '$4,000', 'superbox-full-5.jpg', 0),
(6, 'Decore 6', 'Decore 6 d', 1, '2013-12-27 17:19:22', NULL, '2013/12/27/decore-6/6/', '$4,000', 'superbox-full-6.jpg', 0),
(7, 'Decore 7', 'Decore 7', 1, '2013-12-27 17:20:05', NULL, '2013/12/27/decore-7/7/', '$4,000', 'superbox-full-7.jpg', 0),
(8, 'Decore 8', 'Decore 8', 1, '2013-12-27 17:20:05', NULL, '2013/12/27/decore-8/8/', '$4,000', 'superbox-full-8.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL,
  `slides_name` varchar(255) DEFAULT NULL,
  `slides_caption` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `created_by`, `date_created`, `date_updated`, `slides_name`, `slides_caption`) VALUES
(1, 1, '2013-12-24 17:45:51', NULL, 'new1.jpg', 'Text caption 1'),
(3, 1, '2013-12-24 17:45:51', NULL, 'new3.jpg', 'Text caption 3'),
(4, 1, '2013-12-24 17:45:51', NULL, 'new4.jpg', 'Text caption 4'),
(5, 1, '2013-12-24 17:45:51', NULL, 'new5.jpg', 'Text caption 5'),
(6, 1, '2013-12-24 17:46:08', NULL, 'new6.jpg', 'Text caption 6'),
(13, 1, '2013-12-24 21:57:13', NULL, '1387897033-1.jpg', 'as'),
(14, 1, '2013-12-24 21:59:14', NULL, '1387897154-1.jpg', 'as'),
(15, 1, '2013-12-24 22:08:57', NULL, '1387897737-1.jpg', 'sa');

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE IF NOT EXISTS `social_media` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `target` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `icon` varchar(64) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `social_media`
--

INSERT INTO `social_media` (`id`, `name`, `target`, `date_created`, `date_updated`, `created_by`, `icon`, `status`) VALUES
(1, 'digg', 'http://digg.com/', '2013-12-26 20:48:54', '2013-12-26 21:37:37', 1, 'digg.png', 0),
(2, 'dribble', 'http://dribble.com/', '2013-12-26 20:48:54', '2013-12-26 21:37:37', 1, 'dribbble.png', 0),
(3, 'facebook', 'http://facebook.com/', '2013-12-26 20:48:54', '2013-12-26 21:37:37', 1, 'facebook.png', 1),
(4, 'flickr', 'http://www.flickr.com/', '2013-12-26 20:48:54', '2013-12-26 21:37:37', 1, 'flickr.png', 0),
(5, 'forrst', 'http://forrst.com/', '2013-12-26 20:48:54', '2013-12-26 21:37:37', 1, 'forrst.png', 0),
(6, 'googleplus', 'https://plus.google.com/', '2013-12-26 20:48:54', '2013-12-26 21:37:37', 1, 'google+.png', 1),
(7, 'rss', 'https://www.rss.com/', '2013-12-26 20:48:54', '2013-12-26 21:37:37', 1, 'rss.png', 1),
(8, 'sharethis', 'http://www.sharethis.com/', '2013-12-26 20:48:54', '2013-12-26 21:37:37', 1, 'share-this.png', 0),
(9, 'skype', 'http://www.skype.com/', '2013-12-26 20:48:54', '2013-12-26 21:37:37', 1, 'skype.png', 0),
(10, 'twitter', 'http://twitter.com/', '2013-12-26 20:48:54', '2013-12-26 21:37:37', 1, 'twitter.png', 1),
(11, 'vimeo', 'https://vimeo.com/', '2013-12-26 20:50:29', '2013-12-26 21:37:37', 1, 'vimeo.png', 1),
(12, 'youtube', 'http://www.youtube.com/amisyatours', '2013-12-26 20:50:29', '2013-12-26 21:37:37', 1, 'you_tube.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `heading` varchar(255) DEFAULT NULL,
  `content` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rating` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `testimonials`
--


-- --------------------------------------------------------

--
-- Table structure for table `umrah`
--

CREATE TABLE IF NOT EXISTS `umrah` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `created_by` int(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `umrah`
--

INSERT INTO `umrah` (`id`, `name`, `content`, `created_by`, `date_created`, `date_updated`) VALUES
(1, 'about', '<h3><span style="color: #000000;">Pengertian Umroh</span></h3>\r\n<p><img src="http://amisyatours.com/images/stories/Makkah_by_TKD_Queen.jpg" alt="" border="0" /></p>\r\n<p><span style="color: #000000;"><strong>Umroh</strong> adalah berkunjung ke Ka`bah untuk melakukan serangkaian ibadah dengan syarat-syarat yang telah ditetapkan. Umroh disunahkan bagi muslim yang mampu. Umroh dapat dilakukan kapan saja, kecuali pada hari Arafah yaitu tgl 10 Zulhijah dan hari-hari Tasyrik yaitu tgl 11,12,13 Zulhijah. Melaksanakan Umroh pada bulan Ramadhan sama nilainya dengan melakukan Ibadah Haji (Hadits Muslim)</span></p>\r\n<p><span style="color: #000000;"><strong>Rangkaian Kegiatan Ibadah Umroh</strong></span></p>\r\n<ol>\r\n<li><span style="color: #000000;">Diawali dengan mandi besar (janabah) sebelum ihram untuk umrah.</span></li>\r\n<li><span style="color: #000000;">mengenakan pakaian ihram. Untuk lelaki 2 kain yang dijadikan sarung dan selendang, sedangkan untuk wanita memakai pakaian apa saja yang menutup aurat tanpa ada hiasannya dan tidak memakai cadar atau sarung tangan.</span></li>\r\n<li><span style="color: #000000;">Niat umrah dalam hati dan mengucapkan Labbaika &lsquo;umrotan atau Labbaikallahumma bi`umrotin. Kemudian bertalbiyah dengan dikeraskan suaranya bagi laki-laki dan cukup dengan suara yang didengar orang yang ada di sampingnya bagi wanita, yaitu mengucapkan Labbaikallahumma labbaik labbaika laa syarika laka labbaik. Innal hamda wan ni`mata laka wal mulk laa syarika laka.</span></li>\r\n<li><span style="color: #000000;">Sesampai Masjidil Haram menuju ka`bah, lakukan thawaf sebanyak 7 kali putaran.3 putaran pertama jalan cepat dan sisanya jalan biasa. Thowaf diawali dan diakhiri di hajar aswad dan ka`bah dijadikan berada di sebelah kiri. Setiap putaran menuju hajar aswad sambil menyentuhnya dengan tangan kanan dan menciumnya jika mampu dan mengucapkan Bismillahi wallahu akbar. Jika tidak bisa menyentuh dan menciumya, maka cukup memberi isyarat dan berkata Allahu akbar.</span></li>\r\n<li><span style="color: #000000;">Shalat 2 raka`at di belakang maqam Ibrahim jika bisa atau di tempat lainnya di masjidil haram dengan membaca surat Al-Kafirun pada raka`at pertama dan Al-Ikhlas pada raka`at kedua.</span></li>\r\n<li><span style="color: #000000;">Selanjutnya Sa`i dengan naik ke bukit Shofa dan menghadap kiblat sambil mengangkat kedua tangan dan mengucapkan Innash shofa wal marwata min sya`aairillah. Abda`u bima bada`allahu bihi (Aku memulai dengan apa yang Allah memulainya). Kemudian bertakbir 3 kali tanpa memberi isyarat dan mengucapkan Laa ilaha illallahu wahdahu laa syarika lahu. Lahul mulku wa lahul hamdu wahuwa &lsquo;alaa kulli syai`in qodiir. Laa ilaha illallahu wahdahu anjaza wa`dahu wa shodaqo &lsquo;abdahu wa hazamal ahzaaba wahdahu 3x. Kemudian berdoa sekehendaknya. Sa`i dilakukan sebanyak 7 kali dengan hitungan berangkat satu kali dan kembalinya dihitung satu kali, diawali di bukit Shofa dan diakhiri di bukit Marwah.</span></li>\r\n<li><span style="color: #000000;">Mencukur rambut kepala bagi lelaki dan memotongnya sebatas ujung jari bagi wanita.</span></li>\r\n<li><span style="color: #000000;">Ibadah Umroh selesai</span></li>\r\n</ol>\r\n<div>\r\n<h3><span style="color: #000000;">Persiapan Ibadah</span></h3>\r\n</div>\r\n<p><span style="color: #000000;"><strong>Perlengkapan Pria</strong></span></p>\r\n<ol>\r\n<li><span style="color: #000000;">Kain Ihram dua stel</span></li>\r\n<li><span style="color: #000000;">Baju sehari-hari secukupnya</span></li>\r\n<li><span style="color: #000000;">Ikat pinggang</span></li>\r\n<li><span style="color: #000000;">Keperluan mandi</span></li>\r\n</ol>\r\n<p><span style="color: #000000;"><strong>Perlengkapan Wanita</strong></span></p>\r\n<ol>\r\n<li><span style="color: #000000;">Mukena minimal 2 buah</span></li>\r\n<li><span style="color: #000000;">Pakaian ihram (rok putih dan mukena atas putih) 2 set</span></li>\r\n<li><span style="color: #000000;">Pakaian sehari-hari secukupnya</span></li>\r\n<li><span style="color: #000000;">Kaos kaki secukupnya</span></li>\r\n</ol>\r\n<p><span style="color: #000000;"><strong>Perlengkapan untuk Pria dan Wanita</strong></span></p>\r\n<ol>\r\n<li><span style="color: #000000;">Pakaian penghangat</span></li>\r\n<li><span style="color: #000000;">Selimut</span></li>\r\n<li><span style="color: #000000;">Sandal jepit</span></li>\r\n<li><span style="color: #000000;">Sepatu sandal atau sendal gunung</span></li>\r\n<li><span style="color: #000000;">Obat-obatan pribadi</span></li>\r\n<li><span style="color: #000000;">Gunting kecil utk Tahallul</span></li>\r\n<li><span style="color: #000000;">Payung</span></li>\r\n<li><span style="color: #000000;">Senter kecil (untuk penerangan saat mengambil batu di Musdalifah)</span></li>\r\n<li><span style="color: #000000;">Kantong kecil untuk menyimpan batu kerikil persiapan melempar jumroh</span></li>\r\n<li><span style="color: #000000;">Kantong sandal untuk tempat sandal saat di Masjid</span></li>\r\n<li><span style="color: #000000;">Pelembab atau cream, gunakan untuk tangan dan kaki</span></li>\r\n<li><span style="color: #000000;">Biaya untuk dam, kurban dsb.</span></li>\r\n</ol>\r\n<h3><span style="color: #000000;">Lokasi Utama Ibadah Haji dan Umrah</span></h3>\r\n<p><span style="color: #000000;"><strong>Makkah Al Mukaromah</strong><br /> Di kota Makkah Al-Mukaromah inilah terdapat Masjidil Haram yang didalamnya terdapat Ka`bah yang merupakan kiblat ibadah umat Islam sedunia. Dalam rangkaian perjalanan ibadah haji, Makkah menjadi tempat pembuka dan penutup ibadah haji.</span></p>\r\n<p><span style="color: #000000;"><strong>Padang Arafah</strong><br /> Padang Arafah terdapat di sebelah timur Kota Makkah. Padang Arafah dikenal sebagai tempat pusatnya haji, sebagai tempat pelaksanaan ibadah wukuf yang merupakan rukun haji. Di Padang Arafah juga terdapat Jabal Rahmah tempat pertama kali pertemuan Nabi Adam dan Hawa. Di luar musim haji, daerah ini tidak dipakai.</span></p>\r\n<p><span style="color: #000000;"><strong>Kota Muzdalifah</strong><br /> Kota ini tidak jauh dari kota Mina dan Arafah Mota Muzdalifah merupakan tempat jamaah calon haji melakukan Mabit (bermalam) dan mengambil batu untuk melontar Jumroh di Kota Mina.</span></p>\r\n<p><span style="color: #000000;"><strong>Kota Mina</strong><br /> Kota Mina merupakan tempat berdirinya tugu (jumrah), yaitu tempat pelaksanaan melontarkan batu ke tugu (jumrah) sebagai simbolisasi tindakan nabi Ibrahim ketika mengusir setan. Disana terdapat tiga jumrah yaitu jumrah Aqabah, Jumrah Ula, dan Jumrah Wustha.</span></p>', 1, '2013-12-25 14:03:21', '2013-12-25 14:10:29'),
(2, 'persyaratan', '<p>&nbsp;</p>\r\n<h2>Ketentuan &amp; Syarat Umrah</h2>\r\n<p><span style="color: #000000;"><strong>PERSYARATAN PENDAFTARAN HAJI KHUSUS</strong></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">1. Paspor RI (Hijau) masih berlaku minimal 7 bulan, dengan nama minimal terdiri dari 3 (tiga) kata, misalnya Muhammad Al-Fattih Umar<br /></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">2. Pas Photo berwarna dengan latar belakang PUTIH, ukuran fokus wajah 80% dengan jumlah 4x6 = 8 lembar<br /></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">3. Surat/Buku Nikah asli<br /></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">4. Akte Kelahiran ASLI bagi anak yang berangkat dengan orang tua<br /></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">5. Kartu Kuning Vaksinasi Meningitis</span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">6. Kartu Keluarga ASLI<br /></span></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;"><strong>Biaya Sudah Termasuk :</strong></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">- Pengurusan Visa<br /></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">- Tiket Pesawat pulang pergi kelas ekonomi<br /></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">- Bimbingan manasik umrah<br /></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">- Selama di tanah suci disediakan muthawif<br /></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">- Akomodasi, Konsumsi, Tour dan Transportasi<br /></span></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;"><strong>Biaya Belum Termasuk :</strong></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">- Vaksinasi Meningitis<br /></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">- Surat Mahram bagi jamaah wanita yang berangkat sendiri dan berusia dibawah 45 tahun<br /></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">- Kelebihan bagasi max. 25 kg<br /></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="color: #000000;">- Ongkos tandu/kursi dorong ketika Thawaf Sa''i bagi yang tidak kuat<br /></span></p>\r\n<p><span style="color: #000000;">- Pembuatan pasport atau penambahan nama dibelakang paspor</span></p>', 1, '2013-12-25 14:03:21', '2013-12-25 14:09:08'),
(3, 'biaya', '<p style="text-align: center;"><span style="color: #000000;"><span style="font-size: medium;"><strong>Harga Umrah 2013</strong></span></span></p>\r\n<table border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td rowspan="2" valign="top" width="114">\r\n<p style="text-align: left;">Jadwal januari-desember 2013</p>\r\n</td>\r\n<td colspan="2" valign="top" width="289">\r\n<p align="center">AKOMODASI</p>\r\n</td>\r\n<td rowspan="2" width="109">\r\n<p align="center">Harga</p>\r\n</td>\r\n<td rowspan="2" width="104">\r\n<p align="center">Ket</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign="top" width="110">\r\n<p style="text-align: left;">MEKKAH</p>\r\n</td>\r\n<td valign="top" width="179">\r\n<p style="text-align: left;">MADINAH</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="114">\r\n<p style="text-align: left;">Paket ekonomis</p>\r\n</td>\r\n<td valign="top" width="110">\r\n<p style="text-align: left;">&nbsp;</p>\r\n<p style="text-align: left;">Zuwwar Albayt/Apartemen/Hotel Setaraf*2 (jarak 1 Km)</p>\r\n</td>\r\n<td valign="top" width="179">\r\n<p style="text-align: left;">&nbsp;</p>\r\n<p style="text-align: left;">Raudah Suite/Wassel Arrem/Apartemen</p>\r\n<p style="text-align: left;">Hotel setaraf *3</p>\r\n<p style="text-align: left;">1 kamar 4 orang</p>\r\n</td>\r\n<td width="109">\r\n<p align="center">$ 1750</p>\r\n</td>\r\n<td width="104">\r\n<p align="center">&nbsp;</p>\r\n<p align="center">D + $ 200</p>\r\n<p align="center">T + $ 100</p>\r\n<p align="center">&nbsp;</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="114">\r\n<p style="text-align: left;">Paket reguler</p>\r\n</td>\r\n<td valign="top" width="110">\r\n<p>&nbsp;</p>\r\n<p>Shorouq Diyafah/</p>\r\n<p>Hotel setaraf *3</p>\r\n<p>1 kamar 4 orang</p>\r\n</td>\r\n<td valign="top" width="179">\r\n<p style="text-align: left;">&nbsp;</p>\r\n<p style="text-align: left;">Madinah Mobarok Silver/ Andalus Darul Khoir</p>\r\n<p style="text-align: left;">Hotel setaraf *4</p>\r\n<p style="text-align: left;">1 kamar 4 orang</p>\r\n</td>\r\n<td width="109">\r\n<p align="center">$ 1950</p>\r\n</td>\r\n<td width="104">\r\n<p align="center">&nbsp;</p>\r\n<p align="center">D + $ 250</p>\r\n<p align="center">T + $ 125</p>\r\n<p align="center">&nbsp;</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="114">\r\n<p style="text-align: left;">Paket Eksekutif</p>\r\n</td>\r\n<td valign="top" width="110">\r\n<p style="text-align: left;">&nbsp;</p>\r\n<p style="text-align: left;">Mekkah Zam-zam</p>\r\n<p style="text-align: left;">Hotel setaraf *5</p>\r\n<p style="text-align: left;">1 kamar 4 orang</p>\r\n</td>\r\n<td valign="top" width="179">\r\n<p style="text-align: left;">&nbsp;</p>\r\n<p style="text-align: left;">Madinah Al Haram</p>\r\n<p style="text-align: left;">Hotel setaraf *5</p>\r\n<p style="text-align: left;">1 kamar 4 orang</p>\r\n</td>\r\n<td width="109">\r\n<p align="center">$ 2350</p>\r\n<p align="center">&nbsp;</p>\r\n</td>\r\n<td width="104">\r\n<p align="center">&nbsp;</p>\r\n<p align="center">D + $ 350</p>\r\n<p align="center">T + $ 175</p>\r\n<p align="center">&nbsp;</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="114">\r\n<p style="text-align: left;">Paket umroh</p>\r\n</td>\r\n<td valign="top" width="110">\r\n<p style="text-align: left;">&nbsp;</p>\r\n<p style="text-align: left;">Shorouq diyafah/</p>\r\n<p style="text-align: left;">Hotel setaraf *3</p>\r\n<p style="text-align: left;">1 kamar 4 orang</p>\r\n</td>\r\n<td valign="top" width="179">\r\n<p style="text-align: left;">&nbsp;</p>\r\n<p style="text-align: left;">Madinah Mubarok silver/Andalus Darul Khoir</p>\r\n<p style="text-align: left;">Hotel setaraf *4</p>\r\n<p style="text-align: left;">I kamar 4 orang</p>\r\n</td>\r\n<td width="109">\r\n<p align="center">$ 2850</p>\r\n</td>\r\n<td width="104">\r\n<p align="center">&nbsp;</p>\r\n<p align="center">D + $ 300</p>\r\n<p align="center">T + $ 150</p>\r\n<p style="text-align: left;">&nbsp;</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p style="text-align: left;"><span style="color: #000000; font-weight: bold; line-height: 1.3em;">NOTE:</span></p>\r\n<p style="text-align: left;"><strong><span style="color: #000000;"> * Kami menyelenggarakan Umrah berdasarkan permintaan/incentive group.<br /> * Harga dan jadwal sewaktu-waktu dapat berubah disebabkan oleh adanya perubahan tarif dan jadwal penerbangan.<br /> * Untuk Umrah Ramadhan pendaftaran dan pembayaran sampai 13 Juli 2013<br /> * Airport Tax &amp; Handling (Buku Doa, Kain Ihram (laki-laki)/ Bergo (perempuan), Koper, Tas Tangan, Tas Sandal dll) Rp. 1.000.000,-</span></strong></p>', 1, '2013-12-25 14:03:34', '2013-12-25 14:11:33');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `level` tinyint(1) NOT NULL DEFAULT '1',
  `username` varchar(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `hashed_password` varchar(40) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(40) DEFAULT NULL,
  `profpic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `level`, `username`, `fname`, `lname`, `email`, `password`, `hashed_password`, `date_created`, `date_updated`, `is_active`, `activation_code`, `profpic`) VALUES
(1, 0, 'admin_amisyatours', 'Milan', 'Smith', 'amisyatours@yahoo.com', 'amisyatours', 'a3a0419c451df4dedeb19fcfaf48c5810d35e9a0', '2013-12-24 16:49:47', NULL, 1, 'd25dd8e56e6e1e698156435b8e877a174ed600ef', NULL);
